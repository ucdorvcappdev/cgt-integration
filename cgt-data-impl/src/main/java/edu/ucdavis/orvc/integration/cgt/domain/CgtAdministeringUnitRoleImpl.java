package edu.ucdavis.orvc.integration.cgt.domain;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.joda.time.LocalDateTime;

import edu.ucdavis.orvc.integration.cgt.api.domain.CgtAdministeringUnitRole;
import edu.ucdavis.orvc.integration.cgt.api.domain.dto.CgtAdministeringUnitRoleDTO;



/**
 * Base Hibernate POJO for CGT.
 *
 *
 **/
@Entity
@Table(name = "CGT_AdministeringUnitRoles")
public class CgtAdministeringUnitRoleImpl implements java.io.Serializable, CgtAdministeringUnitRole {

	private static final long serialVersionUID = -8009691094282124602L;

	private int aurUnitRoleKey;
	private String aurName;
	private String aurShortName;
	private Integer aurSortOrder;
	private boolean aurIsActive;
	private String aurUpdatedBy;
	private LocalDateTime aurDateCreated;
	private LocalDateTime aurDateUpdated;

	public CgtAdministeringUnitRoleImpl() {
	}

	public CgtAdministeringUnitRoleImpl(final int aurUnitRoleKey, final String aurName, final boolean aurIsActive, final String aurUpdatedBy,
			final LocalDateTime aurDateCreated, final LocalDateTime aurDateUpdated) {
		this.aurUnitRoleKey = aurUnitRoleKey;
		this.aurName = aurName;
		this.aurIsActive = aurIsActive;
		this.aurUpdatedBy = aurUpdatedBy;
		this.aurDateCreated = aurDateCreated;
		this.aurDateUpdated = aurDateUpdated;
	}

	public CgtAdministeringUnitRoleImpl(final int aurUnitRoleKey, final String aurName, final String aurShortName, final Integer aurSortOrder,
			final boolean aurIsActive, final String aurUpdatedBy, final LocalDateTime aurDateCreated, final LocalDateTime aurDateUpdated) {
		this.aurUnitRoleKey = aurUnitRoleKey;
		this.aurName = aurName;
		this.aurShortName = aurShortName;
		this.aurSortOrder = aurSortOrder;
		this.aurIsActive = aurIsActive;
		this.aurUpdatedBy = aurUpdatedBy;
		this.aurDateCreated = aurDateCreated;
		this.aurDateUpdated = aurDateUpdated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtAdminsteringUnitRole#getAurUnitRoleKey()
	 */
	@Override
	@Id
	@Column(name = "aur_unitRoleKey", unique = true, nullable = false)
	public int getAurUnitRoleKey() {
		return this.aurUnitRoleKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtAdminsteringUnitRole#setAurUnitRoleKey(int)
	 */
	@Override
	public void setAurUnitRoleKey(final int aurUnitRoleKey) {
		this.aurUnitRoleKey = aurUnitRoleKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtAdminsteringUnitRole#getAurName()
	 */
	@Override
	@Column(name = "aur_name", nullable = false)
	public String getAurName() {
		return this.aurName;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtAdminsteringUnitRole#setAurName(java.lang.String)
	 */
	@Override
	public void setAurName(final String aurName) {
		this.aurName = aurName;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtAdminsteringUnitRole#getAurShortName()
	 */
	@Override
	@Column(name = "aur_shortName")
	public String getAurShortName() {
		return this.aurShortName;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtAdminsteringUnitRole#setAurShortName(java.lang.String)
	 */
	@Override
	public void setAurShortName(final String aurShortName) {
		this.aurShortName = aurShortName;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtAdminsteringUnitRole#getAurSortOrder()
	 */
	@Override
	@Column(name = "aur_sortOrder")
	public Integer getAurSortOrder() {
		return this.aurSortOrder;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtAdminsteringUnitRole#setAurSortOrder(java.lang.Integer)
	 */
	@Override
	public void setAurSortOrder(final Integer aurSortOrder) {
		this.aurSortOrder = aurSortOrder;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtAdminsteringUnitRole#isAurIsActive()
	 */
	@Override
	@Column(name = "aur_isActive", nullable = false)
	public boolean isAurIsActive() {
		return this.aurIsActive;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtAdminsteringUnitRole#setAurIsActive(boolean)
	 */
	@Override
	public void setAurIsActive(final boolean aurIsActive) {
		this.aurIsActive = aurIsActive;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtAdminsteringUnitRole#getAurUpdatedBy()
	 */
	@Override
	@Column(name = "aur_updatedBy", nullable = false, length = 35, columnDefinition="char")
	public String getAurUpdatedBy() {
		return this.aurUpdatedBy;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtAdminsteringUnitRole#setAurUpdatedBy(java.lang.String)
	 */
	@Override
	public void setAurUpdatedBy(final String aurUpdatedBy) {
		this.aurUpdatedBy = aurUpdatedBy;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtAdminsteringUnitRole#getAurDateCreated()
	 */
	@Override
	@Type(type="org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
	@Column(name = "aur_dateCreated", nullable = false, length = 23)
	public LocalDateTime getAurDateCreated() {
		return this.aurDateCreated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtAdminsteringUnitRole#setAurDateCreated(org.joda.time.LocalDateTime)
	 */
	@Override
	public void setAurDateCreated(final LocalDateTime aurDateCreated) {
		this.aurDateCreated = aurDateCreated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtAdminsteringUnitRole#getAurDateUpdated()
	 */
	@Override
	@Type(type="org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
	@Column(name = "aur_dateUpdated", nullable = false, length = 23)
	public LocalDateTime getAurDateUpdated() {
		return this.aurDateUpdated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtAdminsteringUnitRole#setAurDateUpdated(org.joda.time.LocalDateTime)
	 */
	@Override
	public void setAurDateUpdated(final LocalDateTime aurDateUpdated) {
		this.aurDateUpdated = aurDateUpdated;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((aurDateCreated == null) ? 0 : aurDateCreated.hashCode());
		result = prime * result + ((aurDateUpdated == null) ? 0 : aurDateUpdated.hashCode());
		result = prime * result + (aurIsActive ? 1231 : 1237);
		result = prime * result + ((aurName == null) ? 0 : aurName.hashCode());
		result = prime * result + ((aurShortName == null) ? 0 : aurShortName.hashCode());
		result = prime * result + ((aurSortOrder == null) ? 0 : aurSortOrder.hashCode());
		result = prime * result + aurUnitRoleKey;
		result = prime * result + ((aurUpdatedBy == null) ? 0 : aurUpdatedBy.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(final Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (!(obj instanceof CgtAdministeringUnitRoleImpl))
			return false;
		final CgtAdministeringUnitRoleImpl other = (CgtAdministeringUnitRoleImpl) obj;
		if (aurDateCreated == null) {
			if (other.aurDateCreated != null)
				return false;
		} else if (!aurDateCreated.equals(other.aurDateCreated))
			return false;
		if (aurDateUpdated == null) {
			if (other.aurDateUpdated != null)
				return false;
		} else if (!aurDateUpdated.equals(other.aurDateUpdated))
			return false;
		if (aurIsActive != other.aurIsActive)
			return false;
		if (aurName == null) {
			if (other.aurName != null)
				return false;
		} else if (!aurName.equals(other.aurName))
			return false;
		if (aurShortName == null) {
			if (other.aurShortName != null)
				return false;
		} else if (!aurShortName.equals(other.aurShortName))
			return false;
		if (aurSortOrder == null) {
			if (other.aurSortOrder != null)
				return false;
		} else if (!aurSortOrder.equals(other.aurSortOrder))
			return false;
		if (aurUnitRoleKey != other.aurUnitRoleKey)
			return false;
		if (aurUpdatedBy == null) {
			if (other.aurUpdatedBy != null)
				return false;
		} else if (!aurUpdatedBy.equals(other.aurUpdatedBy))
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return String.format(
				"CgtAdministeringUnitRoles [aurUnitRoleKey=%s, aurName=%s, aurShortName=%s, aurSortOrder=%s, aurIsActive=%s, aurUpdatedBy=%s, aurDateCreated=%s, aurDateUpdated=%s]",
				aurUnitRoleKey, aurName, aurShortName, aurSortOrder, aurIsActive, aurUpdatedBy, aurDateCreated,
				aurDateUpdated);
	}

	@Override
	public CgtAdministeringUnitRoleDTO toDTO() {
		return new CgtAdministeringUnitRoleDTO(this);
	}

}
