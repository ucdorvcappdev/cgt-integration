package edu.ucdavis.orvc.integration.cgt.domain;

public interface CgtImplDtoConvertable<I,D> {

	public I toImpl();
	
	public D toDTO();
	
}
