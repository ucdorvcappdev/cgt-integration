package edu.ucdavis.orvc.integration.cgt.domain;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.joda.time.LocalDateTime;

import edu.ucdavis.orvc.integration.cgt.api.domain.CgtTransactionType;
import edu.ucdavis.orvc.integration.cgt.api.domain.dto.CgtTransactionTypeDTO;



/**
 * Base Hibernate POJO for CGT.
 *
 *
 **/
@Entity
@Table(name = "CGT_TransactionTypes")
public class CgtTransactionTypeImpl implements CgtTransactionType {

	private static final long serialVersionUID = 156358489294844521L;
	private int tatTransactionTypeKey;
	private String tatName;
	private String tatShortName;
	private Integer tatSortOrder;
	private Boolean tatIsAllowMultipleSubs;
	private boolean tatIsVisible;
	private boolean tatIsActive;
	private String tatUpdatedBy;
	private LocalDateTime tatDateCreated;
	private LocalDateTime tatDateUpdated;

	public CgtTransactionTypeImpl() {
	}

	public CgtTransactionTypeImpl(final int tatTransactionTypeKey, final String tatName, final boolean tatIsVisible, final boolean tatIsActive,
			final String tatUpdatedBy, final LocalDateTime tatDateCreated, final LocalDateTime tatDateUpdated) {
		this.tatTransactionTypeKey = tatTransactionTypeKey;
		this.tatName = tatName;
		this.tatIsVisible = tatIsVisible;
		this.tatIsActive = tatIsActive;
		this.tatUpdatedBy = tatUpdatedBy;
		this.tatDateCreated = tatDateCreated;
		this.tatDateUpdated = tatDateUpdated;
	}

	public CgtTransactionTypeImpl(final int tatTransactionTypeKey, final String tatName, final String tatShortName, final Integer tatSortOrder,
			final Boolean tatIsAllowMultipleSubs, final boolean tatIsVisible, final boolean tatIsActive, final String tatUpdatedBy,
			final LocalDateTime tatDateCreated, final LocalDateTime tatDateUpdated) {
		this.tatTransactionTypeKey = tatTransactionTypeKey;
		this.tatName = tatName;
		this.tatShortName = tatShortName;
		this.tatSortOrder = tatSortOrder;
		this.tatIsAllowMultipleSubs = tatIsAllowMultipleSubs;
		this.tatIsVisible = tatIsVisible;
		this.tatIsActive = tatIsActive;
		this.tatUpdatedBy = tatUpdatedBy;
		this.tatDateCreated = tatDateCreated;
		this.tatDateUpdated = tatDateUpdated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransactionType#getTatTransactionTypeKey()
	 */
	@Override
	@Id
	@Column(name = "tat_transactionTypeKey", unique = true, nullable = false)
	public int getTatTransactionTypeKey() {
		return this.tatTransactionTypeKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransactionType#setTatTransactionTypeKey(int)
	 */
	@Override
	public void setTatTransactionTypeKey(final int tatTransactionTypeKey) {
		this.tatTransactionTypeKey = tatTransactionTypeKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransactionType#getTatName()
	 */
	@Override
	@Column(name = "tat_name", nullable = false)
	public String getTatName() {
		return this.tatName;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransactionType#setTatName(java.lang.String)
	 */
	@Override
	public void setTatName(final String tatName) {
		this.tatName = tatName;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransactionType#getTatShortName()
	 */
	@Override
	@Column(name = "tat_shortName")
	public String getTatShortName() {
		return this.tatShortName;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransactionType#setTatShortName(java.lang.String)
	 */
	@Override
	public void setTatShortName(final String tatShortName) {
		this.tatShortName = tatShortName;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransactionType#getTatSortOrder()
	 */
	@Override
	@Column(name = "tat_sortOrder")
	public Integer getTatSortOrder() {
		return this.tatSortOrder;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransactionType#setTatSortOrder(java.lang.Integer)
	 */
	@Override
	public void setTatSortOrder(final Integer tatSortOrder) {
		this.tatSortOrder = tatSortOrder;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransactionType#getTatIsAllowMultipleSubs()
	 */
	@Override
	@Column(name = "tat_isAllowMultipleSubs")
	public Boolean getTatIsAllowMultipleSubs() {
		return this.tatIsAllowMultipleSubs;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransactionType#setTatIsAllowMultipleSubs(java.lang.Boolean)
	 */
	@Override
	public void setTatIsAllowMultipleSubs(final Boolean tatIsAllowMultipleSubs) {
		this.tatIsAllowMultipleSubs = tatIsAllowMultipleSubs;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransactionType#isTatIsVisible()
	 */
	@Override
	@Column(name = "tat_isVisible", nullable = false)
	public boolean isTatIsVisible() {
		return this.tatIsVisible;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransactionType#setTatIsVisible(boolean)
	 */
	@Override
	public void setTatIsVisible(final boolean tatIsVisible) {
		this.tatIsVisible = tatIsVisible;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransactionType#isTatIsActive()
	 */
	@Override
	@Column(name = "tat_isActive", nullable = false)
	public boolean isTatIsActive() {
		return this.tatIsActive;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransactionType#setTatIsActive(boolean)
	 */
	@Override
	public void setTatIsActive(final boolean tatIsActive) {
		this.tatIsActive = tatIsActive;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransactionType#getTatUpdatedBy()
	 */
	@Override
	@Column(name = "tat_updatedBy", nullable = false, length = 35, columnDefinition="char")
	public String getTatUpdatedBy() {
		return this.tatUpdatedBy;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransactionType#setTatUpdatedBy(java.lang.String)
	 */
	@Override
	public void setTatUpdatedBy(final String tatUpdatedBy) {
		this.tatUpdatedBy = tatUpdatedBy;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransactionType#getTatDateCreated()
	 */
	@Override
	@Type(type="org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
	@Column(name = "tat_dateCreated", nullable = false, length = 23)
	public LocalDateTime getTatDateCreated() {
		return this.tatDateCreated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransactionType#setTatDateCreated(org.joda.time.LocalDateTime)
	 */
	@Override
	public void setTatDateCreated(final LocalDateTime tatDateCreated) {
		this.tatDateCreated = tatDateCreated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransactionType#getTatDateUpdated()
	 */
	@Override
	@Type(type="org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
	@Column(name = "tat_dateUpdated", nullable = false, length = 23)
	public LocalDateTime getTatDateUpdated() {
		return this.tatDateUpdated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransactionType#setTatDateUpdated(org.joda.time.LocalDateTime)
	 */
	@Override
	public void setTatDateUpdated(final LocalDateTime tatDateUpdated) {
		this.tatDateUpdated = tatDateUpdated;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((tatDateCreated == null) ? 0 : tatDateCreated.hashCode());
		result = prime * result + ((tatDateUpdated == null) ? 0 : tatDateUpdated.hashCode());
		result = prime * result + (tatIsActive ? 1231 : 1237);
		result = prime * result + ((tatIsAllowMultipleSubs == null) ? 0 : tatIsAllowMultipleSubs.hashCode());
		result = prime * result + (tatIsVisible ? 1231 : 1237);
		result = prime * result + ((tatName == null) ? 0 : tatName.hashCode());
		result = prime * result + ((tatShortName == null) ? 0 : tatShortName.hashCode());
		result = prime * result + ((tatSortOrder == null) ? 0 : tatSortOrder.hashCode());
		result = prime * result + tatTransactionTypeKey;
		result = prime * result + ((tatUpdatedBy == null) ? 0 : tatUpdatedBy.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(final Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (!(obj instanceof CgtTransactionTypeImpl))
			return false;
		final CgtTransactionTypeImpl other = (CgtTransactionTypeImpl) obj;
		if (tatDateCreated == null) {
			if (other.tatDateCreated != null)
				return false;
		} else if (!tatDateCreated.equals(other.tatDateCreated))
			return false;
		if (tatDateUpdated == null) {
			if (other.tatDateUpdated != null)
				return false;
		} else if (!tatDateUpdated.equals(other.tatDateUpdated))
			return false;
		if (tatIsActive != other.tatIsActive)
			return false;
		if (tatIsAllowMultipleSubs == null) {
			if (other.tatIsAllowMultipleSubs != null)
				return false;
		} else if (!tatIsAllowMultipleSubs.equals(other.tatIsAllowMultipleSubs))
			return false;
		if (tatIsVisible != other.tatIsVisible)
			return false;
		if (tatName == null) {
			if (other.tatName != null)
				return false;
		} else if (!tatName.equals(other.tatName))
			return false;
		if (tatShortName == null) {
			if (other.tatShortName != null)
				return false;
		} else if (!tatShortName.equals(other.tatShortName))
			return false;
		if (tatSortOrder == null) {
			if (other.tatSortOrder != null)
				return false;
		} else if (!tatSortOrder.equals(other.tatSortOrder))
			return false;
		if (tatTransactionTypeKey != other.tatTransactionTypeKey)
			return false;
		if (tatUpdatedBy == null) {
			if (other.tatUpdatedBy != null)
				return false;
		} else if (!tatUpdatedBy.equals(other.tatUpdatedBy))
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return String.format(
				"CgtTransactionTypes [tatTransactionTypeKey=%s, tatName=%s, tatShortName=%s, tatSortOrder=%s, tatIsAllowMultipleSubs=%s, tatIsVisible=%s, tatIsActive=%s, tatUpdatedBy=%s, tatDateCreated=%s, tatDateUpdated=%s]",
				tatTransactionTypeKey, tatName, tatShortName, tatSortOrder, tatIsAllowMultipleSubs, tatIsVisible,
				tatIsActive, tatUpdatedBy, tatDateCreated, tatDateUpdated);
	}



	public CgtTransactionTypeDTO toDTO() {
		return new CgtTransactionTypeDTO(this);
	}

}
