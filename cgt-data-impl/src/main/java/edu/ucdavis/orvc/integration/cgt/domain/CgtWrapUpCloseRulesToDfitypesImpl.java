package edu.ucdavis.orvc.integration.cgt.domain;


import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

import edu.ucdavis.orvc.integration.cgt.api.domain.CgtWrapUpCloseRulesToDfitypes;
import edu.ucdavis.orvc.integration.cgt.api.domain.CgtWrapUpCloseRulesToDfitypesId;
import edu.ucdavis.orvc.integration.cgt.api.domain.dto.CgtWrapUpCloseRulesToDfitypesDTO;

/**
 * Base Hibernate POJO for CGT.
 *
 *
 **/
@Entity
@Table(name = "CGT_WrapUpCloseRulesToDFITypes")
public class CgtWrapUpCloseRulesToDfitypesImpl implements CgtWrapUpCloseRulesToDfitypes {

	private static final long serialVersionUID = 2538086362932402398L;
	private CgtWrapUpCloseRulesToDfitypesIdImpl id;
	private int w2dResultSubmissionTypeKey;

	public CgtWrapUpCloseRulesToDfitypesImpl() {
	}

	public CgtWrapUpCloseRulesToDfitypesImpl(final CgtWrapUpCloseRulesToDfitypesIdImpl id, final int w2dResultSubmissionTypeKey) {
		this.id = id;
		this.w2dResultSubmissionTypeKey = w2dResultSubmissionTypeKey;
	}

	@EmbeddedId
	@AttributeOverrides({
		@AttributeOverride(name = "w2dCloseRuleKey", column = @Column(name = "w2d_closeRuleKey", nullable = false) ),
		@AttributeOverride(name = "w2dDfiTypeKey", column = @Column(name = "w2d_dfiTypeKey", nullable = false) ) })
	public CgtWrapUpCloseRulesToDfitypesIdImpl getIdImpl() {
		return this.id;
	}

	public void setIdImpl(final CgtWrapUpCloseRulesToDfitypesIdImpl id) {
		this.id = id;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtWrapUpCloseRulesToDfitypes#getW2dResultSubmissionTypeKey()
	 */
	@Override
	@Column(name = "w2d_resultSubmissionTypeKey", nullable = false)
	public int getW2dResultSubmissionTypeKey() {
		return this.w2dResultSubmissionTypeKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtWrapUpCloseRulesToDfitypes#setW2dResultSubmissionTypeKey(int)
	 */
	@Override
	public void setW2dResultSubmissionTypeKey(final int w2dResultSubmissionTypeKey) {
		this.w2dResultSubmissionTypeKey = w2dResultSubmissionTypeKey;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + w2dResultSubmissionTypeKey;
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(final Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (!(obj instanceof CgtWrapUpCloseRulesToDfitypesImpl))
			return false;
		final CgtWrapUpCloseRulesToDfitypesImpl other = (CgtWrapUpCloseRulesToDfitypesImpl) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (w2dResultSubmissionTypeKey != other.w2dResultSubmissionTypeKey)
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return String.format("CgtWrapUpCloseRulesToDfitypes [id=%s, w2dResultSubmissionTypeKey=%s]", id,
				w2dResultSubmissionTypeKey);
	}

	@Override
	@Transient
	public CgtWrapUpCloseRulesToDfitypesId getId() {
		return id;
	}

	@Override
	public void setId(CgtWrapUpCloseRulesToDfitypesId id) {
		this.id = (CgtWrapUpCloseRulesToDfitypesIdImpl) id;
	}



	public CgtWrapUpCloseRulesToDfitypesDTO toDTO() {
		return new CgtWrapUpCloseRulesToDfitypesDTO(this);
	}

}
