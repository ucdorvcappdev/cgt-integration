package edu.ucdavis.orvc.integration.cgt.domain;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Type;
import org.joda.time.LocalDateTime;

import edu.ucdavis.orvc.integration.cgt.api.domain.CgtProject;
import edu.ucdavis.orvc.integration.cgt.api.domain.CgtSponsor;
import edu.ucdavis.orvc.integration.cgt.api.domain.CgtSponsorRoleType;
import edu.ucdavis.orvc.integration.cgt.api.domain.MasSponsor;
import edu.ucdavis.orvc.integration.cgt.api.domain.dto.CgtSponsorDTO;



/**
 * Base Hibernate POJO for CGT.
 *
 *
 **/
@Entity
@Table(name = "CGT_Sponsors")
public class CgtSponsorImpl implements CgtSponsor {

	private static final long serialVersionUID = 405078312521794039L;
	private int pspProjectSponsorKey;
	private String pspSponsorKey;
	private String pspProjectKey;
	private Integer pspSponsorRoleKey;
	private String pspCfda;
	private String pspOfficeCode;
	private String pspMisSponsorName;
	private String pspProgramName;
	private String ZPspRootAwardNumber;
	private String pspAddress;
	private String pspContactName;
	private String pspPhone;
	private String pspFax;
	private String pspEmail;
	private boolean pspIsActive;
	private String pspUpdatedBy;
	private LocalDateTime pspDateCreated;
	private LocalDateTime pspDateUpdated;

	private MasSponsorImpl masSponsor;
	private CgtProjectImpl project;
	private CgtSponsorRoleTypeImpl sponsorRoleType;
	
	
	
	public CgtSponsorImpl() {
	}

	public CgtSponsorImpl(final int pspProjectSponsorKey, final String pspProjectKey, final boolean pspIsActive, final String pspUpdatedBy,
			final LocalDateTime pspDateCreated, final LocalDateTime pspDateUpdated) {
		this.pspProjectSponsorKey = pspProjectSponsorKey;
		this.pspProjectKey = pspProjectKey;
		this.pspIsActive = pspIsActive;
		this.pspUpdatedBy = pspUpdatedBy;
		this.pspDateCreated = pspDateCreated;
		this.pspDateUpdated = pspDateUpdated;
	}

	public CgtSponsorImpl(final int pspProjectSponsorKey, final String pspSponsorKey, final String pspProjectKey, final Integer pspSponsorRoleKey,
			final String pspCfda, final String pspOfficeCode, final String pspMisSponsorName, final String pspProgramName,
			final String ZPspRootAwardNumber, final String pspAddress, final String pspContactName, final String pspPhone, final String pspFax,
			final String pspEmail, final boolean pspIsActive, final String pspUpdatedBy, final LocalDateTime pspDateCreated, final LocalDateTime pspDateUpdated) {
		this.pspProjectSponsorKey = pspProjectSponsorKey;
		this.pspSponsorKey = pspSponsorKey;
		this.pspProjectKey = pspProjectKey;
		this.pspSponsorRoleKey = pspSponsorRoleKey;
		this.pspCfda = pspCfda;
		this.pspOfficeCode = pspOfficeCode;
		this.pspMisSponsorName = pspMisSponsorName;
		this.pspProgramName = pspProgramName;
		this.ZPspRootAwardNumber = ZPspRootAwardNumber;
		this.pspAddress = pspAddress;
		this.pspContactName = pspContactName;
		this.pspPhone = pspPhone;
		this.pspFax = pspFax;
		this.pspEmail = pspEmail;
		this.pspIsActive = pspIsActive;
		this.pspUpdatedBy = pspUpdatedBy;
		this.pspDateCreated = pspDateCreated;
		this.pspDateUpdated = pspDateUpdated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtSponosor#getPspProjectSponsorKey()
	 */
	@Override
	@Id
	@Column(name = "psp_projectSponsorKey", unique = true, nullable = false)
	public int getPspProjectSponsorKey() {
		return this.pspProjectSponsorKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtSponosor#setPspProjectSponsorKey(int)
	 */
	@Override
	public void setPspProjectSponsorKey(final int pspProjectSponsorKey) {
		this.pspProjectSponsorKey = pspProjectSponsorKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtSponosor#getPspSponsorKey()
	 */
	@Override
	@Column(name = "psp_sponsorKey", length = 8, columnDefinition = "char")
	public String getPspSponsorKey() {
		return this.pspSponsorKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtSponosor#setPspSponsorKey(java.lang.String)
	 */
	@Override
	public void setPspSponsorKey(final String pspSponsorKey) {
		this.pspSponsorKey = pspSponsorKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtSponosor#getPspProjectKey()
	 */
	@Override
	@Column(name = "psp_projectKey", nullable = false, length = 10)
	public String getPspProjectKey() {
		return this.pspProjectKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtSponosor#setPspProjectKey(java.lang.String)
	 */
	@Override
	public void setPspProjectKey(final String pspProjectKey) {
		this.pspProjectKey = pspProjectKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtSponosor#getPspSponsorRoleKey()
	 */
	@Override
	@Column(name = "psp_sponsorRoleKey")
	public Integer getPspSponsorRoleKey() {
		return this.pspSponsorRoleKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtSponosor#setPspSponsorRoleKey(java.lang.Integer)
	 */
	@Override
	public void setPspSponsorRoleKey(final Integer pspSponsorRoleKey) {
		this.pspSponsorRoleKey = pspSponsorRoleKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtSponosor#getPspCfda()
	 */
	@Override
	@Column(name = "psp_CFDA")
	public String getPspCfda() {
		return this.pspCfda;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtSponosor#setPspCfda(java.lang.String)
	 */
	@Override
	public void setPspCfda(final String pspCfda) {
		this.pspCfda = pspCfda;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtSponosor#getPspOfficeCode()
	 */
	@Override
	@Column(name = "psp_officeCode", length = 6)
	public String getPspOfficeCode() {
		return this.pspOfficeCode;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtSponosor#setPspOfficeCode(java.lang.String)
	 */
	@Override
	public void setPspOfficeCode(final String pspOfficeCode) {
		this.pspOfficeCode = pspOfficeCode;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtSponosor#getPspMisSponsorName()
	 */
	@Override
	@Column(name = "psp_misSponsorName")
	public String getPspMisSponsorName() {
		return this.pspMisSponsorName;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtSponosor#setPspMisSponsorName(java.lang.String)
	 */
	@Override
	public void setPspMisSponsorName(final String pspMisSponsorName) {
		this.pspMisSponsorName = pspMisSponsorName;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtSponosor#getPspProgramName()
	 */
	@Override
	@Column(name = "psp_programName")
	public String getPspProgramName() {
		return this.pspProgramName;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtSponosor#setPspProgramName(java.lang.String)
	 */
	@Override
	public void setPspProgramName(final String pspProgramName) {
		this.pspProgramName = pspProgramName;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtSponosor#getZPspRootAwardNumber()
	 */
	@Override
	@Column(name = "z_psp_rootAwardNumber")
	public String getZPspRootAwardNumber() {
		return this.ZPspRootAwardNumber;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtSponosor#setZPspRootAwardNumber(java.lang.String)
	 */
	@Override
	public void setZPspRootAwardNumber(final String ZPspRootAwardNumber) {
		this.ZPspRootAwardNumber = ZPspRootAwardNumber;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtSponosor#getPspAddress()
	 */
	@Override
	@Column(name = "psp_address")
	public String getPspAddress() {
		return this.pspAddress;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtSponosor#setPspAddress(java.lang.String)
	 */
	@Override
	public void setPspAddress(final String pspAddress) {
		this.pspAddress = pspAddress;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtSponosor#getPspContactName()
	 */
	@Override
	@Column(name = "psp_contactName", length = 50)
	public String getPspContactName() {
		return this.pspContactName;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtSponosor#setPspContactName(java.lang.String)
	 */
	@Override
	public void setPspContactName(final String pspContactName) {
		this.pspContactName = pspContactName;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtSponosor#getPspPhone()
	 */
	@Override
	@Column(name = "psp_phone", length = 50)
	public String getPspPhone() {
		return this.pspPhone;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtSponosor#setPspPhone(java.lang.String)
	 */
	@Override
	public void setPspPhone(final String pspPhone) {
		this.pspPhone = pspPhone;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtSponosor#getPspFax()
	 */
	@Override
	@Column(name = "psp_fax", length = 50)
	public String getPspFax() {
		return this.pspFax;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtSponosor#setPspFax(java.lang.String)
	 */
	@Override
	public void setPspFax(final String pspFax) {
		this.pspFax = pspFax;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtSponosor#getPspEmail()
	 */
	@Override
	@Column(name = "psp_email")
	public String getPspEmail() {
		return this.pspEmail;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtSponosor#setPspEmail(java.lang.String)
	 */
	@Override
	public void setPspEmail(final String pspEmail) {
		this.pspEmail = pspEmail;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtSponosor#isPspIsActive()
	 */
	@Override
	@Column(name = "psp_isActive", nullable = false)
	public boolean isPspIsActive() {
		return this.pspIsActive;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtSponosor#setPspIsActive(boolean)
	 */
	@Override
	public void setPspIsActive(final boolean pspIsActive) {
		this.pspIsActive = pspIsActive;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtSponosor#getPspUpdatedBy()
	 */
	@Override
	@Column(name = "psp_updatedBy", nullable = false, length = 35, columnDefinition="char")
	public String getPspUpdatedBy() {
		return this.pspUpdatedBy;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtSponosor#setPspUpdatedBy(java.lang.String)
	 */
	@Override
	public void setPspUpdatedBy(final String pspUpdatedBy) {
		this.pspUpdatedBy = pspUpdatedBy;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtSponosor#getPspDateCreated()
	 */
	@Override
	@Type(type="org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
	@Column(name = "psp_dateCreated", nullable = false, length = 23)
	public LocalDateTime getPspDateCreated() {
		return this.pspDateCreated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtSponosor#setPspDateCreated(org.joda.time.LocalDateTime)
	 */
	@Override
	public void setPspDateCreated(final LocalDateTime pspDateCreated) {
		this.pspDateCreated = pspDateCreated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtSponosor#getPspDateUpdated()
	 */
	@Override
	@Type(type="org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
	@Column(name = "psp_dateUpdated", nullable = false, length = 23)
	public LocalDateTime getPspDateUpdated() {
		return this.pspDateUpdated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtSponosor#setPspDateUpdated(org.joda.time.LocalDateTime)
	 */
	@Override
	public void setPspDateUpdated(final LocalDateTime pspDateUpdated) {
		this.pspDateUpdated = pspDateUpdated;
	}


	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((ZPspRootAwardNumber == null) ? 0 : ZPspRootAwardNumber.hashCode());
		result = prime * result + ((pspAddress == null) ? 0 : pspAddress.hashCode());
		result = prime * result + ((pspCfda == null) ? 0 : pspCfda.hashCode());
		result = prime * result + ((pspContactName == null) ? 0 : pspContactName.hashCode());
		result = prime * result + ((pspDateCreated == null) ? 0 : pspDateCreated.hashCode());
		result = prime * result + ((pspDateUpdated == null) ? 0 : pspDateUpdated.hashCode());
		result = prime * result + ((pspEmail == null) ? 0 : pspEmail.hashCode());
		result = prime * result + ((pspFax == null) ? 0 : pspFax.hashCode());
		result = prime * result + (pspIsActive ? 1231 : 1237);
		result = prime * result + ((pspMisSponsorName == null) ? 0 : pspMisSponsorName.hashCode());
		result = prime * result + ((pspOfficeCode == null) ? 0 : pspOfficeCode.hashCode());
		result = prime * result + ((pspPhone == null) ? 0 : pspPhone.hashCode());
		result = prime * result + ((pspProgramName == null) ? 0 : pspProgramName.hashCode());
		result = prime * result + ((pspProjectKey == null) ? 0 : pspProjectKey.hashCode());
		result = prime * result + pspProjectSponsorKey;
		result = prime * result + ((pspSponsorKey == null) ? 0 : pspSponsorKey.hashCode());
		result = prime * result + ((pspSponsorRoleKey == null) ? 0 : pspSponsorRoleKey.hashCode());
		result = prime * result + ((pspUpdatedBy == null) ? 0 : pspUpdatedBy.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (!(obj instanceof CgtSponsorImpl)) {
			return false;
		}
		CgtSponsorImpl other = (CgtSponsorImpl) obj;
		if (ZPspRootAwardNumber == null) {
			if (other.ZPspRootAwardNumber != null) {
				return false;
			}
		} else if (!ZPspRootAwardNumber.equals(other.ZPspRootAwardNumber)) {
			return false;
		}
		if (pspAddress == null) {
			if (other.pspAddress != null) {
				return false;
			}
		} else if (!pspAddress.equals(other.pspAddress)) {
			return false;
		}
		if (pspCfda == null) {
			if (other.pspCfda != null) {
				return false;
			}
		} else if (!pspCfda.equals(other.pspCfda)) {
			return false;
		}
		if (pspContactName == null) {
			if (other.pspContactName != null) {
				return false;
			}
		} else if (!pspContactName.equals(other.pspContactName)) {
			return false;
		}
		if (pspDateCreated == null) {
			if (other.pspDateCreated != null) {
				return false;
			}
		} else if (!pspDateCreated.equals(other.pspDateCreated)) {
			return false;
		}
		if (pspDateUpdated == null) {
			if (other.pspDateUpdated != null) {
				return false;
			}
		} else if (!pspDateUpdated.equals(other.pspDateUpdated)) {
			return false;
		}
		if (pspEmail == null) {
			if (other.pspEmail != null) {
				return false;
			}
		} else if (!pspEmail.equals(other.pspEmail)) {
			return false;
		}
		if (pspFax == null) {
			if (other.pspFax != null) {
				return false;
			}
		} else if (!pspFax.equals(other.pspFax)) {
			return false;
		}
		if (pspIsActive != other.pspIsActive) {
			return false;
		}
		if (pspMisSponsorName == null) {
			if (other.pspMisSponsorName != null) {
				return false;
			}
		} else if (!pspMisSponsorName.equals(other.pspMisSponsorName)) {
			return false;
		}
		if (pspOfficeCode == null) {
			if (other.pspOfficeCode != null) {
				return false;
			}
		} else if (!pspOfficeCode.equals(other.pspOfficeCode)) {
			return false;
		}
		if (pspPhone == null) {
			if (other.pspPhone != null) {
				return false;
			}
		} else if (!pspPhone.equals(other.pspPhone)) {
			return false;
		}
		if (pspProgramName == null) {
			if (other.pspProgramName != null) {
				return false;
			}
		} else if (!pspProgramName.equals(other.pspProgramName)) {
			return false;
		}
		if (pspProjectKey == null) {
			if (other.pspProjectKey != null) {
				return false;
			}
		} else if (!pspProjectKey.equals(other.pspProjectKey)) {
			return false;
		}
		if (pspProjectSponsorKey != other.pspProjectSponsorKey) {
			return false;
		}
		if (pspSponsorKey == null) {
			if (other.pspSponsorKey != null) {
				return false;
			}
		} else if (!pspSponsorKey.equals(other.pspSponsorKey)) {
			return false;
		}
		if (pspSponsorRoleKey == null) {
			if (other.pspSponsorRoleKey != null) {
				return false;
			}
		} else if (!pspSponsorRoleKey.equals(other.pspSponsorRoleKey)) {
			return false;
		}
		if (pspUpdatedBy == null) {
			if (other.pspUpdatedBy != null) {
				return false;
			}
		} else if (!pspUpdatedBy.equals(other.pspUpdatedBy)) {
			return false;
		}
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return String.format(
				"CgtSponsor [pspProjectSponsorKey=%s, pspSponsorKey=%s, pspProjectKey=%s, pspSponsorRoleKey=%s, pspCfda=%s, pspOfficeCode=%s, pspMisSponsorName=%s, pspProgramName=%s, ZPspRootAwardNumber=%s, pspAddress=%s, pspContactName=%s, pspPhone=%s, pspFax=%s, pspEmail=%s, pspIsActive=%s, pspUpdatedBy=%s, pspDateCreated=%s, pspDateUpdated=%s, sponsor=%s]",
				pspProjectSponsorKey, pspSponsorKey, pspProjectKey, pspSponsorRoleKey, pspCfda, pspOfficeCode,
				pspMisSponsorName, pspProgramName, ZPspRootAwardNumber, pspAddress, pspContactName, pspPhone, pspFax,
				pspEmail, pspIsActive, pspUpdatedBy, pspDateCreated, pspDateUpdated, masSponsor);
	}

	
	@ManyToOne
	@JoinColumn(name="psp_projectKey",updatable=false,insertable=false)
	public CgtProjectImpl getProjectImpl() {
		return project;
	}


	public void setProjectImpl(CgtProjectImpl project) {
		this.project = project;
	}

	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="psp_sponsorkey")
	public MasSponsorImpl getMasSponsorImpl() {
		return masSponsor;
	}

	public void setMasSponsorImpl(MasSponsorImpl sponsor) {
		this.masSponsor = sponsor;
	}

	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="psp_sponsorrolekey",updatable=false,insertable=false)
	public CgtSponsorRoleTypeImpl getSponsorRoleTypeImpl() {
		return sponsorRoleType;
	}

	public void setSponsorRoleTypeImpl(CgtSponsorRoleTypeImpl sponsorRoleType) {
		this.sponsorRoleType = sponsorRoleType;
	}

	
	@Override
	@Transient
	public CgtProject getProject() {
		return this.project;
	}

	@Override
	public void setProject(CgtProject project) {
		this.project = (CgtProjectImpl) project;
	}

	@Override
	@Transient
	public MasSponsor getMasSponsor() {
		return this.masSponsor;
	}

	@Override
	public void setMasSponsor(MasSponsor sponsor) {
		this.masSponsor = (MasSponsorImpl) sponsor;
	}

	@Override
	@Transient
	public CgtSponsorRoleType getSponsorRoleType() {
		return sponsorRoleType;
	}

	@Override
	public void setSponsorRoleType(CgtSponsorRoleType sponsorRoleType) {
		this.sponsorRoleType = (CgtSponsorRoleTypeImpl)sponsorRoleType;
	}

	public CgtSponsorDTO toDTO() {
		return new CgtSponsorDTO(this);
	}

}
