package edu.ucdavis.orvc.integration.cgt.data.dao;

import java.util.List;

import edu.ucdavis.orvc.integration.cgt.api.domain.CgtProject;
import edu.ucdavis.orvc.integration.cgt.domain.CgtProjectImpl;

public interface CgtProjectDao {

	List<CgtProjectImpl> findAllByProjectNumber(String projectNumber);

	CgtProject findByProjectNumber(String projectNumber);
	
	void logProjectInfo(String projectNumber);

	CgtProject findActiveByProjectNumber(String projectNumber);

	/**
	 * @param projectNumber
	 * @param areActive
	 * @return
	 * 
	 * Method will load all projects that are CGA eligible, or a single project if it is cga eligible
	 * when projectNumber is specified. If the areActive flag is set then only active projects will be returned.
	 * 
	 * 
	 * 
	 */
	List<CgtProjectImpl> findCgaFeedEligibleProjects(String projectNumber, boolean areActive);
	
	/**
	 * @param projectNumber
	 * @param isActive
	 * @return
	 * 
	 * Returns the CGT project record if it is eligible for the CGA feed.  if the isActive flag is set then the project
	 * must be active to be returned.
	 * 
	 */
	CgtProject getCgaEligibleProject(String projectNumber, boolean isActive);
}