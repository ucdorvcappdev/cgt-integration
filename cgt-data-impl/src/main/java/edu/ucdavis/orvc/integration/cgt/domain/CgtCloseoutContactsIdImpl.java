package edu.ucdavis.orvc.integration.cgt.domain;


import javax.persistence.Column;
import javax.persistence.Embeddable;

import edu.ucdavis.orvc.integration.cgt.api.domain.CgtCloseoutContactsId;
import edu.ucdavis.orvc.integration.cgt.api.domain.dto.CgtCloseoutContactsIdDTO;

/**
 * Base Hibernate POJO for CGT.
 *
 *
 **/
@Embeddable
public class CgtCloseoutContactsIdImpl implements CgtCloseoutContactsId {

	private static final long serialVersionUID = 4626152721442315630L;

	private String ccUserKey;
	private String ccMothraId;
	private String ccAdminDept;
	private Integer ccRole;
	private String ccRoleName;

	public CgtCloseoutContactsIdImpl() {
	}

	public CgtCloseoutContactsIdImpl(final String ccUserKey) {
		this.ccUserKey = ccUserKey;
	}

	public CgtCloseoutContactsIdImpl(final String ccUserKey, final String ccMothraId, final String ccAdminDept, final Integer ccRole,
			final String ccRoleName) {
		this.ccUserKey = ccUserKey;
		this.ccMothraId = ccMothraId;
		this.ccAdminDept = ccAdminDept;
		this.ccRole = ccRole;
		this.ccRoleName = ccRoleName;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtCloseoutContactsId#getCcUserKey()
	 */
	@Override
	@Column(name = "cc_userKey", nullable = false, length = 35,columnDefinition="char")
	public String getCcUserKey() {
		return this.ccUserKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtCloseoutContactsId#setCcUserKey(java.lang.String)
	 */
	@Override
	public void setCcUserKey(final String ccUserKey) {
		this.ccUserKey = ccUserKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtCloseoutContactsId#getCcMothraId()
	 */
	@Override
	@Column(name = "cc_mothraID", length = 8)
	public String getCcMothraId() {
		return this.ccMothraId;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtCloseoutContactsId#setCcMothraId(java.lang.String)
	 */
	@Override
	public void setCcMothraId(final String ccMothraId) {
		this.ccMothraId = ccMothraId;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtCloseoutContactsId#getCcAdminDept()
	 */
	@Override
	@Column(name = "cc_adminDept", length = 10)
	public String getCcAdminDept() {
		return this.ccAdminDept;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtCloseoutContactsId#setCcAdminDept(java.lang.String)
	 */
	@Override
	public void setCcAdminDept(final String ccAdminDept) {
		this.ccAdminDept = ccAdminDept;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtCloseoutContactsId#getCcRole()
	 */
	@Override
	@Column(name = "cc_role")
	public Integer getCcRole() {
		return this.ccRole;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtCloseoutContactsId#setCcRole(java.lang.Integer)
	 */
	@Override
	public void setCcRole(final Integer ccRole) {
		this.ccRole = ccRole;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtCloseoutContactsId#getCcRoleName()
	 */
	@Override
	@Column(name = "cc_role_name", length = 20)
	public String getCcRoleName() {
		return this.ccRoleName;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtCloseoutContactsId#setCcRoleName(java.lang.String)
	 */
	@Override
	public void setCcRoleName(final String ccRoleName) {
		this.ccRoleName = ccRoleName;
	}

	@Override
	public boolean equals(final Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof CgtCloseoutContactsIdImpl))
			return false;
		final CgtCloseoutContactsId castOther = (CgtCloseoutContactsId) other;

		return ((this.getCcUserKey() == castOther.getCcUserKey()) || (this.getCcUserKey() != null
				&& castOther.getCcUserKey() != null && this.getCcUserKey().equals(castOther.getCcUserKey())))
				&& ((this.getCcMothraId() == castOther.getCcMothraId()) || (this.getCcMothraId() != null
				&& castOther.getCcMothraId() != null && this.getCcMothraId().equals(castOther.getCcMothraId())))
				&& ((this.getCcAdminDept() == castOther.getCcAdminDept())
						|| (this.getCcAdminDept() != null && castOther.getCcAdminDept() != null
						&& this.getCcAdminDept().equals(castOther.getCcAdminDept())))
				&& ((this.getCcRole() == castOther.getCcRole()) || (this.getCcRole() != null
				&& castOther.getCcRole() != null && this.getCcRole().equals(castOther.getCcRole())))
				&& ((this.getCcRoleName() == castOther.getCcRoleName())
						|| (this.getCcRoleName() != null && castOther.getCcRoleName() != null
						&& this.getCcRoleName().equals(castOther.getCcRoleName())));
	}

	@Override
	public int hashCode() {
		int result = 17;

		result = 37 * result + (getCcUserKey() == null ? 0 : this.getCcUserKey().hashCode());
		result = 37 * result + (getCcMothraId() == null ? 0 : this.getCcMothraId().hashCode());
		result = 37 * result + (getCcAdminDept() == null ? 0 : this.getCcAdminDept().hashCode());
		result = 37 * result + (getCcRole() == null ? 0 : this.getCcRole().hashCode());
		result = 37 * result + (getCcRoleName() == null ? 0 : this.getCcRoleName().hashCode());
		return result;
	}

	public CgtCloseoutContactsIdDTO toDTO() {
		return new CgtCloseoutContactsIdDTO(this);
	}
}
