package edu.ucdavis.orvc.integration.cgt.api.domain;

import edu.ucdavis.orvc.integration.cgt.api.domain.dto.CgtEmailTemplatesToeDocsTypesDTO;

public interface CgtEmailTemplatesToeDocsTypes extends CgtBaseInterface<CgtEmailTemplatesToeDocsTypesDTO> { 

	int getE2dId();

	void setE2dId(int e2dId);

	Integer getE2dEmailTemplateKey();

	void setE2dEmailTemplateKey(Integer e2dEmailTemplateKey);

	int getE2dEDocsTypeKey();

	void setE2dEDocsTypeKey(int e2dEDocsTypeKey);

	boolean isE2dIsRequired();

	void setE2dIsRequired(boolean e2dIsRequired);

	Integer getE2dParentTransactionTypeKey();

	void setE2dParentTransactionTypeKey(Integer e2dParentTransactionTypeKey);

	Integer getE2dParentTransactionGroupTypeKey();

	void setE2dParentTransactionGroupTypeKey(Integer e2dParentTransactionGroupTypeKey);

}