package edu.ucdavis.orvc.integration.cgt.api.domain;

import edu.ucdavis.orvc.integration.cgt.api.domain.dto.CgtEDocsMessagesToFilesIdDTO;

public interface CgtEDocsMessagesToFilesId extends CgtBaseInterface<CgtEDocsMessagesToFilesIdDTO>
 {

	String getEdfEDocKey();

	void setEdfEDocKey(String edfEDocKey);

	int getEdfFileKey();

	void setEdfFileKey(int edfFileKey);

}