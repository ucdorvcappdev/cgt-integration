package edu.ucdavis.orvc.integration.cgt.api.domain;

import java.math.BigDecimal;

import org.joda.time.LocalDateTime;

import edu.ucdavis.orvc.integration.cgt.api.domain.dto.CgtTransactionDTO;

public interface CgtTransaction extends CgtBaseInterface<CgtTransactionDTO> { 

	int getPtrTransactionKey();

	void setPtrTransactionKey(int ptrTransactionKey);

	Integer getPtrParentTransactionKey();

	void setPtrParentTransactionKey(Integer ptrParentTransactionKey);

	Integer getPtrTransactionGroupTypeKey();

	void setPtrTransactionGroupTypeKey(Integer ptrTransactionGroupTypeKey);

	Integer getPtrTransactionGroupSequenceNumber();

	void setPtrTransactionGroupSequenceNumber(Integer ptrTransactionGroupSequenceNumber);

	String getPtrTransactionGroupSubContractorKey();

	void setPtrTransactionGroupSubContractorKey(String ptrTransactionGroupSubContractorKey);

	String getPtrTransactionGroupNameOld();

	void setPtrTransactionGroupNameOld(String ptrTransactionGroupNameOld);

	String getPtrTransactionGroupName();

	void setPtrTransactionGroupName(String ptrTransactionGroupName);

	String getPtrProjectKey();

	void setPtrProjectKey(String ptrProjectKey);

	int getPtrTransactionTypeKey();

	void setPtrTransactionTypeKey(int ptrTransactionTypeKey);

	int getPtrSubmissionTypeKey();

	void setPtrSubmissionTypeKey(int ptrSubmissionTypeKey);

	LocalDateTime getPtrTransactionStart();

	void setPtrTransactionStart(LocalDateTime ptrTransactionStart);

	LocalDateTime getPtrTransactionEnd();

	void setPtrTransactionEnd(LocalDateTime ptrTransactionEnd);

	BigDecimal getPtrDirectAmount();

	void setPtrDirectAmount(BigDecimal ptrDirectAmount);

	BigDecimal getPtrIndirectAmount();

	void setPtrIndirectAmount(BigDecimal ptrIndirectAmount);

	BigDecimal getPtrIcr1();

	void setPtrIcr1(BigDecimal ptrIcr1);

	Integer getPtrBaseRate1key();

	void setPtrBaseRate1key(Integer ptrBaseRate1key);

	BigDecimal getPtrIcr2();

	void setPtrIcr2(BigDecimal ptrIcr2);

	Integer getPtrBaseRate2key();

	void setPtrBaseRate2key(Integer ptrBaseRate2key);

	Boolean getPtrIsIcrwaiver();

	void setPtrIsIcrwaiver(Boolean ptrIsIcrwaiver);

	String getPtrWaiverNumber();

	void setPtrWaiverNumber(String ptrWaiverNumber);

	BigDecimal getPtrCostShareAmount();

	void setPtrCostShareAmount(BigDecimal ptrCostShareAmount);

	String getPtrOpfundNumber();

	void setPtrOpfundNumber(String ptrOpfundNumber);

	String getPtrSponsorAwardNumber();

	void setPtrSponsorAwardNumber(String ptrSponsorAwardNumber);

	String getPtrNotes();

	void setPtrNotes(String ptrNotes);

	String getPtrSurveyKey();

	void setPtrSurveyKey(String ptrSurveyKey);

	boolean isPtrIsActive();

	void setPtrIsActive(boolean ptrIsActive);

	String getPtrUpdatedBy();

	void setPtrUpdatedBy(String ptrUpdatedBy);

	LocalDateTime getPtrDateUpdated();

	void setPtrDateUpdated(LocalDateTime ptrDateUpdated);

	LocalDateTime getPtrDateCreated();

	void setPtrDateCreated(LocalDateTime ptrDateCreated);

	String getPtrUniqueId();

	void setPtrUniqueId(String ptrUniqueId);

	String getPtrEfadocumentNumber();

	void setPtrEfadocumentNumber(String ptrEfadocumentNumber);

	String getPtrEfafailedReason();

	void setPtrEfafailedReason(String ptrEfafailedReason);

	LocalDateTime getPtrEfadateProcessed();

	void setPtrEfadateProcessed(LocalDateTime ptrEfadateProcessed);
	
	CgtProject getProject();

	void setProject(CgtProject project);

	/**
	 * @return the transactionSubmissionType
	 */

	CgtTransactionSubmissionType getTransactionSubmissionType();

	/**
	 * @param transactionSubmissionType the transactionSubmissionType to set
	 */
	void setTransactionSubmissionType(CgtTransactionSubmissionType transactionSubmissionType);

	/**
	 * @return the transactionType
	 */
	CgtTransactionType getTransactionType();

	/**
	 * @param transactionType the transactionType to set
	 */
	void setTransactionType(CgtTransactionType transactionType);

	/**
	 * @return the transactionGroupType
	 */

	CgtTransactionGroupType getTransactionGroupType();

	/**
	 * @param transactionGroupType the transactionGroupType to set
	 */
	void setTransactionGroupType(CgtTransactionGroupType transactionGroupType);

}