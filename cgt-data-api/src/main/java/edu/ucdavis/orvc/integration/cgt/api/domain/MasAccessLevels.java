package edu.ucdavis.orvc.integration.cgt.api.domain;

import org.joda.time.LocalDateTime;

import edu.ucdavis.orvc.integration.cgt.api.domain.dto.MasAccessLevelsDTO;

public interface MasAccessLevels extends CgtBaseInterface<MasAccessLevelsDTO> { 

	int getAclAccessLevelKey();

	void setAclAccessLevelKey(int aclAccessLevelKey);

	String getAclDataSourceKey();

	void setAclDataSourceKey(String aclDataSourceKey);

	String getAclUserKey();

	void setAclUserKey(String aclUserKey);

	LocalDateTime getAclDateCreated();

	void setAclDateCreated(LocalDateTime aclDateCreated);

	LocalDateTime getAclDateUpdated();

	void setAclDateUpdated(LocalDateTime aclDateUpdated);

	String getAclUdatedBy();

	void setAclUdatedBy(String aclUdatedBy);

}