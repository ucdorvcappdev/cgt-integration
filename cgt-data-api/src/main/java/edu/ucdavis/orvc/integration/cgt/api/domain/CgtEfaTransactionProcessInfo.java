package edu.ucdavis.orvc.integration.cgt.api.domain;

import org.joda.time.LocalDateTime;

import edu.ucdavis.orvc.integration.cgt.api.domain.dto.CgtEfaTransactionProcessInfoDTO;

public interface CgtEfaTransactionProcessInfo extends CgtBaseInterface<CgtEfaTransactionProcessInfoDTO> { 

	void setPtrProjectKey(String ptrProjectKey);

	String getPtrProjectKey();

	void setPtrParentTransactionKey(Integer ptrParentTransactionKey);

	Integer getPtrParentTransactionKey();

	void setPtrTransactionKey(int ptrTransactionKey);

	int getPtrTransactionKey();

	void setEfadateProcessed(LocalDateTime efadateProcessed);

	LocalDateTime getEfadateProcessed();

	void setEfafailedReason(String efafailedReason);

	String getEfafailedReason();

	void setEfadocumentNumber(String efadocumentNumber);

	String getEfadocumentNumber();

	
	
	
	
	
}
