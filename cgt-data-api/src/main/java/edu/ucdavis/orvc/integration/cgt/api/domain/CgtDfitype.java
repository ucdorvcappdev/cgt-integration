package edu.ucdavis.orvc.integration.cgt.api.domain;

import org.joda.time.LocalDateTime;

import edu.ucdavis.orvc.integration.cgt.api.domain.dto.CgtDfitypeDTO;

public interface CgtDfitype extends CgtBaseInterface<CgtDfitypeDTO> { 

	int getDftDfitypeKey();

	void setDftDfitypeKey(int dftDfitypeKey);

	String getDftName();

	void setDftName(String dftName);

	String getDftShortName();

	void setDftShortName(String dftShortName);

	Integer getDftSortOrder();

	void setDftSortOrder(Integer dftSortOrder);

	boolean isDftIsActive();

	void setDftIsActive(boolean dftIsActive);

	String getDftUpdatedBy();

	void setDftUpdatedBy(String dftUpdatedBy);

	LocalDateTime getDftDateCreated();

	void setDftDateCreated(LocalDateTime dftDateCreated);

	LocalDateTime getDftDateUpdated();

	void setDftDateUpdated(LocalDateTime dftDateUpdated);

}