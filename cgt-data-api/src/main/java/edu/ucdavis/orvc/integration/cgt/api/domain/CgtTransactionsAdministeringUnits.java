package edu.ucdavis.orvc.integration.cgt.api.domain;

import org.joda.time.LocalDateTime;

import edu.ucdavis.orvc.integration.cgt.api.domain.dto.CgtTransactionsAdministeringUnitsDTO;

public interface CgtTransactionsAdministeringUnits extends CgtBaseInterface<CgtTransactionsAdministeringUnitsDTO> { 

	int getTauTransactionAdministeringUnitKey();

	void setTauTransactionAdministeringUnitKey(int tauTransactionAdministeringUnitKey);

	int getTauTransactionKey();

	void setTauTransactionKey(int tauTransactionKey);

	int getTauAdministeringUnitKey();

	void setTauAdministeringUnitKey(int tauAdministeringUnitKey);

	long getTauAllocation();

	void setTauAllocation(long tauAllocation);

	boolean isTauIsActive();

	void setTauIsActive(boolean tauIsActive);

	String getTauUpdatedBy();

	void setTauUpdatedBy(String tauUpdatedBy);

	LocalDateTime getTauDateCreated();

	void setTauDateCreated(LocalDateTime tauDateCreated);

	LocalDateTime getTauDateUpdated();

	void setTauDateUpdated(LocalDateTime tauDateUpdated);

}