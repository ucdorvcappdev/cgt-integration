package edu.ucdavis.orvc.integration.cgt.api.domain.dto;


import org.joda.time.LocalDateTime;

import edu.ucdavis.orvc.integration.cgt.api.domain.CgtProjectReceivedTypes;



/**
 * Base Hibernate POJO for CGT.
 *
 *
 **/


public class CgtProjectReceivedTypesDTO implements CgtProjectReceivedTypes {

	private static final long serialVersionUID = -2127724693338700416L;
	private int prrProjectReceivedTypeKey;
	private String prrName;
	private String prrShortName;
	private Integer prrSortOrder;
	private Boolean prrIsVisible;
	private Boolean prrIsActive;
	private String prrUpdatedBy;
	private LocalDateTime prrDateCreated;
	private LocalDateTime prrDateUpdated;

	public CgtProjectReceivedTypesDTO(final CgtProjectReceivedTypes fromObj) {



	}

	public CgtProjectReceivedTypesDTO() {
	}

	public CgtProjectReceivedTypesDTO(final int prrProjectReceivedTypeKey, final String prrName) {
		this.prrProjectReceivedTypeKey = prrProjectReceivedTypeKey;
		this.prrName = prrName;
	}

	public CgtProjectReceivedTypesDTO(final int prrProjectReceivedTypeKey, final String prrName, final String prrShortName,
			final Integer prrSortOrder, final Boolean prrIsVisible, final Boolean prrIsActive, final String prrUpdatedBy, final LocalDateTime prrDateCreated,
			final LocalDateTime prrDateUpdated) {
		this.prrProjectReceivedTypeKey = prrProjectReceivedTypeKey;
		this.prrName = prrName;
		this.prrShortName = prrShortName;
		this.prrSortOrder = prrSortOrder;
		this.prrIsVisible = prrIsVisible;
		this.prrIsActive = prrIsActive;
		this.prrUpdatedBy = prrUpdatedBy;
		this.prrDateCreated = prrDateCreated;
		this.prrDateUpdated = prrDateUpdated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtProjectReceivedTypes#getPrrProjectReceivedTypeKey()
	 */
	@Override
	

	
	public int getPrrProjectReceivedTypeKey() {
		return this.prrProjectReceivedTypeKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtProjectReceivedTypes#setPrrProjectReceivedTypeKey(int)
	 */
	@Override
	public void setPrrProjectReceivedTypeKey(final int prrProjectReceivedTypeKey) {
		this.prrProjectReceivedTypeKey = prrProjectReceivedTypeKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtProjectReceivedTypes#getPrrName()
	 */
	@Override
	
	public String getPrrName() {
		return this.prrName;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtProjectReceivedTypes#setPrrName(java.lang.String)
	 */
	@Override
	public void setPrrName(final String prrName) {
		this.prrName = prrName;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtProjectReceivedTypes#getPrrShortName()
	 */
	@Override
	
	public String getPrrShortName() {
		return this.prrShortName;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtProjectReceivedTypes#setPrrShortName(java.lang.String)
	 */
	@Override
	public void setPrrShortName(final String prrShortName) {
		this.prrShortName = prrShortName;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtProjectReceivedTypes#getPrrSortOrder()
	 */
	@Override
	
	public Integer getPrrSortOrder() {
		return this.prrSortOrder;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtProjectReceivedTypes#setPrrSortOrder(java.lang.Integer)
	 */
	@Override
	public void setPrrSortOrder(final Integer prrSortOrder) {
		this.prrSortOrder = prrSortOrder;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtProjectReceivedTypes#getPrrIsVisible()
	 */
	@Override
	
	public Boolean getPrrIsVisible() {
		return this.prrIsVisible;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtProjectReceivedTypes#setPrrIsVisible(java.lang.Boolean)
	 */
	@Override
	public void setPrrIsVisible(final Boolean prrIsVisible) {
		this.prrIsVisible = prrIsVisible;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtProjectReceivedTypes#getPrrIsActive()
	 */
	@Override
	
	public Boolean getPrrIsActive() {
		return this.prrIsActive;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtProjectReceivedTypes#setPrrIsActive(java.lang.Boolean)
	 */
	@Override
	public void setPrrIsActive(final Boolean prrIsActive) {
		this.prrIsActive = prrIsActive;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtProjectReceivedTypes#getPrrUpdatedBy()
	 */
	@Override
	
	public String getPrrUpdatedBy() {
		return this.prrUpdatedBy;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtProjectReceivedTypes#setPrrUpdatedBy(java.lang.String)
	 */
	@Override
	public void setPrrUpdatedBy(final String prrUpdatedBy) {
		this.prrUpdatedBy = prrUpdatedBy;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtProjectReceivedTypes#getPrrDateCreated()
	 */
	@Override
	
	
	public LocalDateTime getPrrDateCreated() {
		return this.prrDateCreated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtProjectReceivedTypes#setPrrDateCreated(org.joda.time.LocalDateTime)
	 */
	@Override
	public void setPrrDateCreated(final LocalDateTime prrDateCreated) {
		this.prrDateCreated = prrDateCreated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtProjectReceivedTypes#getPrrDateUpdated()
	 */
	@Override
	
	
	public LocalDateTime getPrrDateUpdated() {
		return this.prrDateUpdated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtProjectReceivedTypes#setPrrDateUpdated(org.joda.time.LocalDateTime)
	 */
	@Override
	public void setPrrDateUpdated(final LocalDateTime prrDateUpdated) {
		this.prrDateUpdated = prrDateUpdated;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((prrDateCreated == null) ? 0 : prrDateCreated.hashCode());
		result = prime * result + ((prrDateUpdated == null) ? 0 : prrDateUpdated.hashCode());
		result = prime * result + ((prrIsActive == null) ? 0 : prrIsActive.hashCode());
		result = prime * result + ((prrIsVisible == null) ? 0 : prrIsVisible.hashCode());
		result = prime * result + ((prrName == null) ? 0 : prrName.hashCode());
		result = prime * result + prrProjectReceivedTypeKey;
		result = prime * result + ((prrShortName == null) ? 0 : prrShortName.hashCode());
		result = prime * result + ((prrSortOrder == null) ? 0 : prrSortOrder.hashCode());
		result = prime * result + ((prrUpdatedBy == null) ? 0 : prrUpdatedBy.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (!(obj instanceof CgtProjectReceivedTypesDTO)) {
			return false;
		}
		CgtProjectReceivedTypesDTO other = (CgtProjectReceivedTypesDTO) obj;
		if (prrDateCreated == null) {
			if (other.prrDateCreated != null) {
				return false;
			}
		} else if (!prrDateCreated.equals(other.prrDateCreated)) {
			return false;
		}
		if (prrDateUpdated == null) {
			if (other.prrDateUpdated != null) {
				return false;
			}
		} else if (!prrDateUpdated.equals(other.prrDateUpdated)) {
			return false;
		}
		if (prrIsActive == null) {
			if (other.prrIsActive != null) {
				return false;
			}
		} else if (!prrIsActive.equals(other.prrIsActive)) {
			return false;
		}
		if (prrIsVisible == null) {
			if (other.prrIsVisible != null) {
				return false;
			}
		} else if (!prrIsVisible.equals(other.prrIsVisible)) {
			return false;
		}
		if (prrName == null) {
			if (other.prrName != null) {
				return false;
			}
		} else if (!prrName.equals(other.prrName)) {
			return false;
		}
		if (prrProjectReceivedTypeKey != other.prrProjectReceivedTypeKey) {
			return false;
		}
		if (prrShortName == null) {
			if (other.prrShortName != null) {
				return false;
			}
		} else if (!prrShortName.equals(other.prrShortName)) {
			return false;
		}
		if (prrSortOrder == null) {
			if (other.prrSortOrder != null) {
				return false;
			}
		} else if (!prrSortOrder.equals(other.prrSortOrder)) {
			return false;
		}
		if (prrUpdatedBy == null) {
			if (other.prrUpdatedBy != null) {
				return false;
			}
		} else if (!prrUpdatedBy.equals(other.prrUpdatedBy)) {
			return false;
		}
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return String.format(
				"CgtProjectReceivedTypesImpl [prrProjectReceivedTypeKey=%s, prrName=%s, prrShortName=%s, prrSortOrder=%s, prrIsVisible=%s, prrIsActive=%s, prrUpdatedBy=%s, prrDateCreated=%s, prrDateUpdated=%s]",
				prrProjectReceivedTypeKey, prrName, prrShortName, prrSortOrder, prrIsVisible, prrIsActive, prrUpdatedBy,
				prrDateCreated, prrDateUpdated);
	}

	public CgtProjectReceivedTypesDTO toDTO() {
		return new CgtProjectReceivedTypesDTO(this);
	}
}
