package edu.ucdavis.orvc.integration.cgt.api.domain;

import org.joda.time.LocalDateTime;

import edu.ucdavis.orvc.integration.cgt.api.domain.dto.CgtTransactionTypeDTO;

public interface CgtTransactionType extends CgtBaseInterface<CgtTransactionTypeDTO> { 

	int getTatTransactionTypeKey();

	void setTatTransactionTypeKey(int tatTransactionTypeKey);

	String getTatName();

	void setTatName(String tatName);

	String getTatShortName();

	void setTatShortName(String tatShortName);

	Integer getTatSortOrder();

	void setTatSortOrder(Integer tatSortOrder);

	Boolean getTatIsAllowMultipleSubs();

	void setTatIsAllowMultipleSubs(Boolean tatIsAllowMultipleSubs);

	boolean isTatIsVisible();

	void setTatIsVisible(boolean tatIsVisible);

	boolean isTatIsActive();

	void setTatIsActive(boolean tatIsActive);

	String getTatUpdatedBy();

	void setTatUpdatedBy(String tatUpdatedBy);

	LocalDateTime getTatDateCreated();

	void setTatDateCreated(LocalDateTime tatDateCreated);

	LocalDateTime getTatDateUpdated();

	void setTatDateUpdated(LocalDateTime tatDateUpdated);

}