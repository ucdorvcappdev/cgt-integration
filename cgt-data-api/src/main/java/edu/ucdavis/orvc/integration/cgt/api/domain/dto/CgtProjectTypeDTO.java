package edu.ucdavis.orvc.integration.cgt.api.domain.dto;


import org.joda.time.LocalDateTime;

import edu.ucdavis.orvc.integration.cgt.api.domain.CgtProjectType;



/**
 * Base Hibernate POJO for CGT.
 *
 *
 **/


public class CgtProjectTypeDTO implements CgtProjectType {

	private static final long serialVersionUID = 6464543426738772212L;
	private int prtProjectTypeKey;
	private String prtName;
	private String prtShortName;
	private Integer prtSortOrder;
	private Boolean prtIsActive;
	private String prtUpdatedBy;
	private LocalDateTime prtDateCreated;
	private LocalDateTime prtDateUpdated;

	public CgtProjectTypeDTO(final CgtProjectType fromObj) {



	}

	public CgtProjectTypeDTO() {
	}

	public CgtProjectTypeDTO(final int prtProjectTypeKey, final String prtName) {
		this.prtProjectTypeKey = prtProjectTypeKey;
		this.prtName = prtName;
	}

	public CgtProjectTypeDTO(final int prtProjectTypeKey, final String prtName, final String prtShortName, final Integer prtSortOrder,
			final Boolean prtIsActive, final String prtUpdatedBy, final LocalDateTime prtDateCreated, final LocalDateTime prtDateUpdated) {
		this.prtProjectTypeKey = prtProjectTypeKey;
		this.prtName = prtName;
		this.prtShortName = prtShortName;
		this.prtSortOrder = prtSortOrder;
		this.prtIsActive = prtIsActive;
		this.prtUpdatedBy = prtUpdatedBy;
		this.prtDateCreated = prtDateCreated;
		this.prtDateUpdated = prtDateUpdated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtProjectType#getPrtProjectTypeKey()
	 */
	@Override
	

	
	public int getPrtProjectTypeKey() {
		return this.prtProjectTypeKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtProjectType#setPrtProjectTypeKey(int)
	 */
	@Override
	public void setPrtProjectTypeKey(final int prtProjectTypeKey) {
		this.prtProjectTypeKey = prtProjectTypeKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtProjectType#getPrtName()
	 */
	@Override
	
	public String getPrtName() {
		return this.prtName;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtProjectType#setPrtName(java.lang.String)
	 */
	@Override
	public void setPrtName(final String prtName) {
		this.prtName = prtName;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtProjectType#getPrtShortName()
	 */
	@Override
	
	public String getPrtShortName() {
		return this.prtShortName;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtProjectType#setPrtShortName(java.lang.String)
	 */
	@Override
	public void setPrtShortName(final String prtShortName) {
		this.prtShortName = prtShortName;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtProjectType#getPrtSortOrder()
	 */
	@Override
	
	public Integer getPrtSortOrder() {
		return this.prtSortOrder;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtProjectType#setPrtSortOrder(java.lang.Integer)
	 */
	@Override
	public void setPrtSortOrder(final Integer prtSortOrder) {
		this.prtSortOrder = prtSortOrder;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtProjectType#getPrtIsActive()
	 */
	@Override
	
	public Boolean getPrtIsActive() {
		return this.prtIsActive;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtProjectType#setPrtIsActive(java.lang.Boolean)
	 */
	@Override
	public void setPrtIsActive(final Boolean prtIsActive) {
		this.prtIsActive = prtIsActive;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtProjectType#getPrtUpdatedBy()
	 */
	@Override
	
	public String getPrtUpdatedBy() {
		return this.prtUpdatedBy;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtProjectType#setPrtUpdatedBy(java.lang.String)
	 */
	@Override
	public void setPrtUpdatedBy(final String prtUpdatedBy) {
		this.prtUpdatedBy = prtUpdatedBy;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtProjectType#getPrtDateCreated()
	 */
	@Override
	
	
	public LocalDateTime getPrtDateCreated() {
		return this.prtDateCreated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtProjectType#setPrtDateCreated(org.joda.time.LocalDateTime)
	 */
	@Override
	public void setPrtDateCreated(final LocalDateTime prtDateCreated) {
		this.prtDateCreated = prtDateCreated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtProjectType#getPrtDateUpdated()
	 */
	@Override
	
	
	public LocalDateTime getPrtDateUpdated() {
		return this.prtDateUpdated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtProjectType#setPrtDateUpdated(org.joda.time.LocalDateTime)
	 */
	@Override
	public void setPrtDateUpdated(final LocalDateTime prtDateUpdated) {
		this.prtDateUpdated = prtDateUpdated;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return String.format(
				"CgtProjectType [prtProjectTypeKey=%s, prtName=%s, prtShortName=%s, prtSortOrder=%s, prtIsActive=%s, prtUpdatedBy=%s, prtDateCreated=%s, prtDateUpdated=%s]",
				prtProjectTypeKey, prtName, prtShortName, prtSortOrder, prtIsActive, prtUpdatedBy, prtDateCreated,
				prtDateUpdated);
	}

	public CgtProjectTypeDTO toDTO() {
		return new CgtProjectTypeDTO(this);
	}
}
