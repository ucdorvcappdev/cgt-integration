package edu.ucdavis.orvc.integration.cgt.api.domain;

import org.joda.time.LocalDateTime;

import edu.ucdavis.orvc.integration.cgt.api.domain.dto.CgtProjectNumbersDTO;

public interface CgtProjectNumbers extends CgtBaseInterface<CgtProjectNumbersDTO> { 

	int getPrnProjectNumber();

	void setPrnProjectNumber(int prnProjectNumber);

	boolean isPrnIsActive();

	void setPrnIsActive(boolean prnIsActive);

	String getPrnUpdatedBy();

	void setPrnUpdatedBy(String prnUpdatedBy);

	LocalDateTime getPrnDateCreated();

	void setPrnDateCreated(LocalDateTime prnDateCreated);

	LocalDateTime getPrnDateUpdated();

	void setPrnDateUpdated(LocalDateTime prnDateUpdated);

}