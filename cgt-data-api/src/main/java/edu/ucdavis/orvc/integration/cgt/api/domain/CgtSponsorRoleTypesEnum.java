package edu.ucdavis.orvc.integration.cgt.api.domain;

public enum CgtSponsorRoleTypesEnum {

	PRIMARY (1,"Primary"),
	ORIGINATING (2,"Originating");
	
	public int typeKey;
	public String shortName;
	
	private CgtSponsorRoleTypesEnum(int typeKey,String shortName) {
		this.typeKey = typeKey;
		this.shortName = shortName;
	}


}
