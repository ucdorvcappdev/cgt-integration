package edu.ucdavis.orvc.integration.cgt.api.domain;

import org.joda.time.LocalDateTime;

import edu.ucdavis.orvc.integration.cgt.api.domain.dto.MasUserPrefixesDTO;

public interface MasUserPrefixes extends CgtBaseInterface<MasUserPrefixesDTO> { 

	int getUspUserPrefixKey();

	void setUspUserPrefixKey(int uspUserPrefixKey);

	String getUspName();

	void setUspName(String uspName);

	LocalDateTime getUspDateCreated();

	void setUspDateCreated(LocalDateTime uspDateCreated);

	LocalDateTime getUspDateUpdated();

	void setUspDateUpdated(LocalDateTime uspDateUpdated);

	String getUspUpdatedBy();

	void setUspUpdatedBy(String uspUpdatedBy);

	boolean isUspIsActive();

	void setUspIsActive(boolean uspIsActive);

}