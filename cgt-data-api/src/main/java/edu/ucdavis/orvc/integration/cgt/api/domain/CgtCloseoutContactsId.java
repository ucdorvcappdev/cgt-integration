package edu.ucdavis.orvc.integration.cgt.api.domain;

import edu.ucdavis.orvc.integration.cgt.api.domain.dto.CgtCloseoutContactsIdDTO;

public interface CgtCloseoutContactsId extends CgtBaseInterface<CgtCloseoutContactsIdDTO> {

	String getCcUserKey();

	void setCcUserKey(String ccUserKey);

	String getCcMothraId();

	void setCcMothraId(String ccMothraId);

	String getCcAdminDept();

	void setCcAdminDept(String ccAdminDept);

	Integer getCcRole();

	void setCcRole(Integer ccRole);

	String getCcRoleName();

	void setCcRoleName(String ccRoleName);

}