package edu.ucdavis.orvc.integration.cgt.api.domain;

import org.joda.time.LocalDateTime;

import edu.ucdavis.orvc.integration.cgt.api.domain.dto.CgtInstrumentTypeDTO;

public interface CgtInstrumentType extends CgtBaseInterface<CgtInstrumentTypeDTO> { 

	int getItyInstrumentTypeKey();

	void setItyInstrumentTypeKey(int ityInstrumentTypeKey);

	String getItyName();

	void setItyName(String ityName);

	String getItyShortName();

	void setItyShortName(String ityShortName);

	Integer getItySortOrder();

	void setItySortOrder(Integer itySortOrder);

	boolean isItyIsActive();

	void setItyIsActive(boolean ityIsActive);

	String getItyUpdatedBy();

	void setItyUpdatedBy(String ityUpdatedBy);

	LocalDateTime getItyDateUpdated();

	void setItyDateUpdated(LocalDateTime ityDateUpdated);

	LocalDateTime getItyDateCreated();

	void setItyDateCreated(LocalDateTime ityDateCreated);

}