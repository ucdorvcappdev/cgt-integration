package edu.ucdavis.orvc.integration.cgt.api.domain;

import org.joda.time.LocalDateTime;

import edu.ucdavis.orvc.integration.cgt.api.domain.dto.CgtEDocsMessagesDTO;

public interface CgtEDocsMessages extends CgtBaseInterface<CgtEDocsMessagesDTO> { 

	String getMsgEDocKey();

	void setMsgEDocKey(String msgEDocKey);

	String getMsgFromUserKey();

	void setMsgFromUserKey(String msgFromUserKey);

	String getMsgSubject();

	void setMsgSubject(String msgSubject);

	String getMsgMessage();

	void setMsgMessage(String msgMessage);

	LocalDateTime getMsgExpirationDate();

	void setMsgExpirationDate(LocalDateTime msgExpirationDate);

	boolean isMsgIsActive();

	void setMsgIsActive(boolean msgIsActive);

	LocalDateTime getMsgDateCreated();

	void setMsgDateCreated(LocalDateTime msgDateCreated);

	LocalDateTime getMsgDateUpdated();

	void setMsgDateUpdated(LocalDateTime msgDateUpdated);

	String getMsgUpdatedBy();

	void setMsgUpdatedBy(String msgUpdatedBy);

}