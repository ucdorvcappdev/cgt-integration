package edu.ucdavis.orvc.integration.cgt.api.domain;

import org.joda.time.LocalDateTime;

import edu.ucdavis.orvc.integration.cgt.api.domain.dto.CgtDeliverableTypesDTO;

public interface CgtDeliverableTypes extends CgtBaseInterface<CgtDeliverableTypesDTO> { 

	int getDetDeliverableTypeKey();

	void setDetDeliverableTypeKey(int detDeliverableTypeKey);

	String getDetName();

	void setDetName(String detName);

	String getDetShortName();

	void setDetShortName(String detShortName);

	Integer getDetSortOrder();

	void setDetSortOrder(Integer detSortOrder);

	Boolean getDetIsActive();

	void setDetIsActive(Boolean detIsActive);

	LocalDateTime getDetDateCreated();

	void setDetDateCreated(LocalDateTime detDateCreated);

	LocalDateTime getDetDateUpdated();

	void setDetDateUpdated(LocalDateTime detDateUpdated);

	String getDetUpdatedBy();

	void setDetUpdatedBy(String detUpdatedBy);

}