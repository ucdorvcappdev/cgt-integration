package edu.ucdavis.orvc.integration.cgt.api.domain;

import java.math.BigDecimal;

import org.joda.time.LocalDateTime;

import edu.ucdavis.orvc.integration.cgt.api.domain.dto.CgtProjectLocationsDTO;

public interface CgtProjectLocations extends CgtBaseInterface<CgtProjectLocationsDTO> { 

	int getPloProjectLocationKey();

	void setPloProjectLocationKey(int ploProjectLocationKey);

	String getPloProjectKey();

	void setPloProjectKey(String ploProjectKey);

	String getPloName();

	void setPloName(String ploName);

	BigDecimal getPloUcowned();

	void setPloUcowned(BigDecimal ploUcowned);

	BigDecimal getPloUcleased();

	void setPloUcleased(BigDecimal ploUcleased);

	BigDecimal getPloOtherUcownedLeased();

	void setPloOtherUcownedLeased(BigDecimal ploOtherUcownedLeased);

	Boolean getPloIsNewLeased();

	void setPloIsNewLeased(Boolean ploIsNewLeased);

	boolean isPloIsActive();

	void setPloIsActive(boolean ploIsActive);

	String getPloUpdatedBy();

	void setPloUpdatedBy(String ploUpdatedBy);

	LocalDateTime getPloDateCreated();

	void setPloDateCreated(LocalDateTime ploDateCreated);

	LocalDateTime getPloDateUpdated();

	void setPloDateUpdated(LocalDateTime ploDateUpdated);

}