package edu.ucdavis.orvc.integration.cgt.api.domain;

import org.joda.time.LocalDateTime;

import edu.ucdavis.orvc.integration.cgt.api.domain.dto.MasUcdbouccodesDTO;

public interface MasUcdbouccodes extends CgtBaseInterface<MasUcdbouccodesDTO> { 

	String getBouCode();

	void setBouCode(String bouCode);

	String getBouShortName();

	void setBouShortName(String bouShortName);

	String getBouLongName();

	void setBouLongName(String bouLongName);

	LocalDateTime getBouDateCreated();

	void setBouDateCreated(LocalDateTime bouDateCreated);

	LocalDateTime getBouDateUpdated();

	void setBouDateUpdated(LocalDateTime bouDateUpdated);

	String getBouUpdatedBy();

	void setBouUpdatedBy(String bouUpdatedBy);

}