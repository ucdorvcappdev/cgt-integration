package edu.ucdavis.orvc.integration.cgt.api.domain.service;

import java.util.List;

import edu.ucdavis.orvc.integration.cgt.api.domain.CgtTransaction;

public interface CgtFeedTransactionInfoService {

	List<CgtTransaction> getUnprocessedTransactions(final int maxDaysOld);

}