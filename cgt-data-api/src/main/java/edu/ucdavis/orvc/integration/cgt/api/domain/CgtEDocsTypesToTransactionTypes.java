package edu.ucdavis.orvc.integration.cgt.api.domain;

import edu.ucdavis.orvc.integration.cgt.api.domain.dto.CgtEDocsTypesToTransactionTypesDTO;

public interface CgtEDocsTypesToTransactionTypes extends CgtBaseInterface<CgtEDocsTypesToTransactionTypesDTO> { 

	CgtEDocsTypesToTransactionTypesId getId();

	void setId(CgtEDocsTypesToTransactionTypesId id);

}