package edu.ucdavis.orvc.integration.cgt.api.domain;

import org.joda.time.LocalDateTime;

import edu.ucdavis.orvc.integration.cgt.api.domain.dto.MasAuthUsersDTO;

public interface MasAuthUsers extends CgtBaseInterface<MasAuthUsersDTO> { 

	int getAuuAuthUserKey();

	void setAuuAuthUserKey(int auuAuthUserKey);

	String getAuuUserName();

	void setAuuUserName(String auuUserName);

	String getAuuIpaddress();

	void setAuuIpaddress(String auuIpaddress);

	String getAuuDataSourceKey();

	void setAuuDataSourceKey(String auuDataSourceKey);

	LocalDateTime getAuuDateCreated();

	void setAuuDateCreated(LocalDateTime auuDateCreated);

	LocalDateTime getAuuDateUpdated();

	void setAuuDateUpdated(LocalDateTime auuDateUpdated);

	String getAuuUpdatedBy();

	void setAuuUpdatedBy(String auuUpdatedBy);

}