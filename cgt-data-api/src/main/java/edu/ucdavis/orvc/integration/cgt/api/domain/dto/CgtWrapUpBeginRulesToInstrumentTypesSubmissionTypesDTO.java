package edu.ucdavis.orvc.integration.cgt.api.domain.dto;


import edu.ucdavis.orvc.integration.cgt.api.domain.CgtWrapUpBeginRulesToInstrumentTypesSubmissionTypes;
import edu.ucdavis.orvc.integration.cgt.api.domain.CgtWrapUpBeginRulesToInstrumentTypesSubmissionTypesId;

/**
 * Base Hibernate POJO for CGT.
 *
 *
 **/


public class CgtWrapUpBeginRulesToInstrumentTypesSubmissionTypesDTO implements CgtWrapUpBeginRulesToInstrumentTypesSubmissionTypes {

	private static final long serialVersionUID = 4293341673371464089L;
	private CgtWrapUpBeginRulesToInstrumentTypesSubmissionTypesIdDTO id;
	private Integer w2iTransactionGroupTypeKey;
	private Integer w2iNotTransactionGroupTypeKey;

	public CgtWrapUpBeginRulesToInstrumentTypesSubmissionTypesDTO(final CgtWrapUpBeginRulesToInstrumentTypesSubmissionTypes fromObj) {



	}

	public CgtWrapUpBeginRulesToInstrumentTypesSubmissionTypesDTO() {
	}

	public CgtWrapUpBeginRulesToInstrumentTypesSubmissionTypesDTO(
			final CgtWrapUpBeginRulesToInstrumentTypesSubmissionTypesIdDTO id) {
		this.id = id;
	}

	public CgtWrapUpBeginRulesToInstrumentTypesSubmissionTypesDTO(final CgtWrapUpBeginRulesToInstrumentTypesSubmissionTypesIdDTO id,
			final Integer w2iTransactionGroupTypeKey, final Integer w2iNotTransactionGroupTypeKey) {
		this.id = id;
		this.w2iTransactionGroupTypeKey = w2iTransactionGroupTypeKey;
		this.w2iNotTransactionGroupTypeKey = w2iNotTransactionGroupTypeKey;
	}

	
	public CgtWrapUpBeginRulesToInstrumentTypesSubmissionTypesIdDTO getIdImpl() {
		return this.id;
	}

	public void setIdImpl(final CgtWrapUpBeginRulesToInstrumentTypesSubmissionTypesIdDTO id) {
		this.id = id;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtWrapUpBeginRulesToInstrumentTypesSubmissionTypes#getW2iTransactionGroupTypeKey()
	 */
	@Override
	
	public Integer getW2iTransactionGroupTypeKey() {
		return this.w2iTransactionGroupTypeKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtWrapUpBeginRulesToInstrumentTypesSubmissionTypes#setW2iTransactionGroupTypeKey(java.lang.Integer)
	 */
	@Override
	public void setW2iTransactionGroupTypeKey(final Integer w2iTransactionGroupTypeKey) {
		this.w2iTransactionGroupTypeKey = w2iTransactionGroupTypeKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtWrapUpBeginRulesToInstrumentTypesSubmissionTypes#getW2iNotTransactionGroupTypeKey()
	 */
	@Override
	
	public Integer getW2iNotTransactionGroupTypeKey() {
		return this.w2iNotTransactionGroupTypeKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtWrapUpBeginRulesToInstrumentTypesSubmissionTypes#setW2iNotTransactionGroupTypeKey(java.lang.Integer)
	 */
	@Override
	public void setW2iNotTransactionGroupTypeKey(final Integer w2iNotTransactionGroupTypeKey) {
		this.w2iNotTransactionGroupTypeKey = w2iNotTransactionGroupTypeKey;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result
				+ ((w2iNotTransactionGroupTypeKey == null) ? 0 : w2iNotTransactionGroupTypeKey.hashCode());
		result = prime * result + ((w2iTransactionGroupTypeKey == null) ? 0 : w2iTransactionGroupTypeKey.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(final Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (!(obj instanceof CgtWrapUpBeginRulesToInstrumentTypesSubmissionTypesDTO))
			return false;
		final CgtWrapUpBeginRulesToInstrumentTypesSubmissionTypesDTO other = (CgtWrapUpBeginRulesToInstrumentTypesSubmissionTypesDTO) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (w2iNotTransactionGroupTypeKey == null) {
			if (other.w2iNotTransactionGroupTypeKey != null)
				return false;
		} else if (!w2iNotTransactionGroupTypeKey.equals(other.w2iNotTransactionGroupTypeKey))
			return false;
		if (w2iTransactionGroupTypeKey == null) {
			if (other.w2iTransactionGroupTypeKey != null)
				return false;
		} else if (!w2iTransactionGroupTypeKey.equals(other.w2iTransactionGroupTypeKey))
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return String.format(
				"CgtWrapUpBeginRulesToInstrumentTypesSubmissionTypes [id=%s, w2iTransactionGroupTypeKey=%s, w2iNotTransactionGroupTypeKey=%s]",
				id, w2iTransactionGroupTypeKey, w2iNotTransactionGroupTypeKey);
	}

	@Override
	public void setId(CgtWrapUpBeginRulesToInstrumentTypesSubmissionTypesId id) {
		this.id = (CgtWrapUpBeginRulesToInstrumentTypesSubmissionTypesIdDTO) id;
	}

	@Override
	
	public CgtWrapUpBeginRulesToInstrumentTypesSubmissionTypesId getId() {
		return this.id;
	}

	public CgtWrapUpBeginRulesToInstrumentTypesSubmissionTypesDTO toDTO() {
		return new CgtWrapUpBeginRulesToInstrumentTypesSubmissionTypesDTO(this);
	}
}
