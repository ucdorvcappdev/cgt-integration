package edu.ucdavis.orvc.integration.cgt.api.domain.dto;


import org.joda.time.LocalDateTime;

import edu.ucdavis.orvc.integration.cgt.api.domain.CgtTransactionsKeyPersonnels;



/**
 * Base Hibernate POJO for CGT.
 *
 *
 **/


public class CgtTransactionsKeyPersonnelsDTO implements CgtTransactionsKeyPersonnels {

	private static final long serialVersionUID = 7245288071463677952L;
	private int tkpTransactionKeyPersonnelKey;
	private int tpkTransactionKey;
	private int tpkKeyPersonnelKey;
	private long tpkAllocation;
	private boolean tpkIsActive;
	private String tpkUpdatedBy;
	private LocalDateTime tpkDateCreated;
	private LocalDateTime tpkDateUpdated;

	public CgtTransactionsKeyPersonnelsDTO(final CgtTransactionsKeyPersonnels fromObj) {



	}

	public CgtTransactionsKeyPersonnelsDTO() {
	}

	public CgtTransactionsKeyPersonnelsDTO(final int tkpTransactionKeyPersonnelKey, final int tpkTransactionKey,
			final int tpkKeyPersonnelKey, final long tpkAllocation, final boolean tpkIsActive, final String tpkUpdatedBy, final LocalDateTime tpkDateCreated,
			final LocalDateTime tpkDateUpdated) {
		this.tkpTransactionKeyPersonnelKey = tkpTransactionKeyPersonnelKey;
		this.tpkTransactionKey = tpkTransactionKey;
		this.tpkKeyPersonnelKey = tpkKeyPersonnelKey;
		this.tpkAllocation = tpkAllocation;
		this.tpkIsActive = tpkIsActive;
		this.tpkUpdatedBy = tpkUpdatedBy;
		this.tpkDateCreated = tpkDateCreated;
		this.tpkDateUpdated = tpkDateUpdated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransactionsKeyPersonnels#getTkpTransactionKeyPersonnelKey()
	 */
	@Override
	

	
	public int getTkpTransactionKeyPersonnelKey() {
		return this.tkpTransactionKeyPersonnelKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransactionsKeyPersonnels#setTkpTransactionKeyPersonnelKey(int)
	 */
	@Override
	public void setTkpTransactionKeyPersonnelKey(final int tkpTransactionKeyPersonnelKey) {
		this.tkpTransactionKeyPersonnelKey = tkpTransactionKeyPersonnelKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransactionsKeyPersonnels#getTpkTransactionKey()
	 */
	@Override
	
	public int getTpkTransactionKey() {
		return this.tpkTransactionKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransactionsKeyPersonnels#setTpkTransactionKey(int)
	 */
	@Override
	public void setTpkTransactionKey(final int tpkTransactionKey) {
		this.tpkTransactionKey = tpkTransactionKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransactionsKeyPersonnels#getTpkKeyPersonnelKey()
	 */
	@Override
	
	public int getTpkKeyPersonnelKey() {
		return this.tpkKeyPersonnelKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransactionsKeyPersonnels#setTpkKeyPersonnelKey(int)
	 */
	@Override
	public void setTpkKeyPersonnelKey(final int tpkKeyPersonnelKey) {
		this.tpkKeyPersonnelKey = tpkKeyPersonnelKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransactionsKeyPersonnels#getTpkAllocation()
	 */
	@Override
	
	public long getTpkAllocation() {
		return this.tpkAllocation;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransactionsKeyPersonnels#setTpkAllocation(long)
	 */
	@Override
	public void setTpkAllocation(final long tpkAllocation) {
		this.tpkAllocation = tpkAllocation;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransactionsKeyPersonnels#isTpkIsActive()
	 */
	@Override
	
	public boolean isTpkIsActive() {
		return this.tpkIsActive;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransactionsKeyPersonnels#setTpkIsActive(boolean)
	 */
	@Override
	public void setTpkIsActive(final boolean tpkIsActive) {
		this.tpkIsActive = tpkIsActive;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransactionsKeyPersonnels#getTpkUpdatedBy()
	 */
	@Override
	
	public String getTpkUpdatedBy() {
		return this.tpkUpdatedBy;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransactionsKeyPersonnels#setTpkUpdatedBy(java.lang.String)
	 */
	@Override
	public void setTpkUpdatedBy(final String tpkUpdatedBy) {
		this.tpkUpdatedBy = tpkUpdatedBy;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransactionsKeyPersonnels#getTpkDateCreated()
	 */
	@Override
	
	
	public LocalDateTime getTpkDateCreated() {
		return this.tpkDateCreated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransactionsKeyPersonnels#setTpkDateCreated(org.joda.time.LocalDateTime)
	 */
	@Override
	public void setTpkDateCreated(final LocalDateTime tpkDateCreated) {
		this.tpkDateCreated = tpkDateCreated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransactionsKeyPersonnels#getTpkDateUpdated()
	 */
	@Override
	
	
	public LocalDateTime getTpkDateUpdated() {
		return this.tpkDateUpdated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtTransactionsKeyPersonnels#setTpkDateUpdated(org.joda.time.LocalDateTime)
	 */
	@Override
	public void setTpkDateUpdated(final LocalDateTime tpkDateUpdated) {
		this.tpkDateUpdated = tpkDateUpdated;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + tkpTransactionKeyPersonnelKey;
		result = prime * result + (int) (tpkAllocation ^ (tpkAllocation >>> 32));
		result = prime * result + ((tpkDateCreated == null) ? 0 : tpkDateCreated.hashCode());
		result = prime * result + ((tpkDateUpdated == null) ? 0 : tpkDateUpdated.hashCode());
		result = prime * result + (tpkIsActive ? 1231 : 1237);
		result = prime * result + tpkKeyPersonnelKey;
		result = prime * result + tpkTransactionKey;
		result = prime * result + ((tpkUpdatedBy == null) ? 0 : tpkUpdatedBy.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(final Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (!(obj instanceof CgtTransactionsKeyPersonnelsDTO))
			return false;
		final CgtTransactionsKeyPersonnelsDTO other = (CgtTransactionsKeyPersonnelsDTO) obj;
		if (tkpTransactionKeyPersonnelKey != other.tkpTransactionKeyPersonnelKey)
			return false;
		if (tpkAllocation != other.tpkAllocation)
			return false;
		if (tpkDateCreated == null) {
			if (other.tpkDateCreated != null)
				return false;
		} else if (!tpkDateCreated.equals(other.tpkDateCreated))
			return false;
		if (tpkDateUpdated == null) {
			if (other.tpkDateUpdated != null)
				return false;
		} else if (!tpkDateUpdated.equals(other.tpkDateUpdated))
			return false;
		if (tpkIsActive != other.tpkIsActive)
			return false;
		if (tpkKeyPersonnelKey != other.tpkKeyPersonnelKey)
			return false;
		if (tpkTransactionKey != other.tpkTransactionKey)
			return false;
		if (tpkUpdatedBy == null) {
			if (other.tpkUpdatedBy != null)
				return false;
		} else if (!tpkUpdatedBy.equals(other.tpkUpdatedBy))
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return String.format(
				"CgtTransactionsKeyPersonnels [tkpTransactionKeyPersonnelKey=%s, tpkTransactionKey=%s, tpkKeyPersonnelKey=%s, tpkAllocation=%s, tpkIsActive=%s, tpkUpdatedBy=%s, tpkDateCreated=%s, tpkDateUpdated=%s]",
				tkpTransactionKeyPersonnelKey, tpkTransactionKey, tpkKeyPersonnelKey, tpkAllocation, tpkIsActive,
				tpkUpdatedBy, tpkDateCreated, tpkDateUpdated);
	}

	public CgtTransactionsKeyPersonnelsDTO toDTO() {
		return new CgtTransactionsKeyPersonnelsDTO(this);
	}
}
