package edu.ucdavis.orvc.integration.cgt.api.domain;

public enum CgtTransactionTypesEnum {

	SYSTEM (1,	"System",	"SYS")
   ,STATUS (2,	"Status", "STA")
   ,AWARD(3,"Award","AWD")
   ,REQUEST(4,"Request","REQ")
   ,DATA_ENTRY(500,"Data Entry","DEN")
   ,PROPOSAL(510,"Proposal","PRO")
   ,POST_PROPOSAL(520,"Post Proposal","PPR")
   ,AWARD530(530,"Award","AWD")
   ,POST_AWARD(540,"Post Award","PAW")
   ,CLOSEOUT(550,"Closeout","CLO")
   ,PILOT_HISTORY(560,"Pilot History","PHI")
   ,SUBAWARDS(570,"Subawards","SUB")
   ,GIFT_PROCESSING(580,"Gift Processing","GIFT")
   ,WRAP_UP(590,"Wrap Up","WUP")
   ,SPARK_PROJECT(900,"SPARK Project","SPPRJ");
	
	public int typeKey;
	public String shortName;
	public String name;
	
	private CgtTransactionTypesEnum(int typeKey,String name, String shortName) {
		this.typeKey = typeKey;
		this.name = name;
		this.shortName = shortName;
	}

}
