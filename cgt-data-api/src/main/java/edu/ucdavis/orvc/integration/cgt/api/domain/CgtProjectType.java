package edu.ucdavis.orvc.integration.cgt.api.domain;

import org.joda.time.LocalDateTime;

import edu.ucdavis.orvc.integration.cgt.api.domain.dto.CgtProjectTypeDTO;

public interface CgtProjectType extends CgtBaseInterface<CgtProjectTypeDTO> { 

	int getPrtProjectTypeKey();

	void setPrtProjectTypeKey(int prtProjectTypeKey);

	String getPrtName();

	void setPrtName(String prtName);

	String getPrtShortName();

	void setPrtShortName(String prtShortName);

	Integer getPrtSortOrder();

	void setPrtSortOrder(Integer prtSortOrder);

	Boolean getPrtIsActive();

	void setPrtIsActive(Boolean prtIsActive);

	String getPrtUpdatedBy();

	void setPrtUpdatedBy(String prtUpdatedBy);

	LocalDateTime getPrtDateCreated();

	void setPrtDateCreated(LocalDateTime prtDateCreated);

	LocalDateTime getPrtDateUpdated();

	void setPrtDateUpdated(LocalDateTime prtDateUpdated);

}