package edu.ucdavis.orvc.integration.cgt.api.domain;

import org.joda.time.LocalDateTime;

import edu.ucdavis.orvc.integration.cgt.api.domain.dto.MasUsersHistoryDTO;

public interface MasUsersHistory extends CgtBaseInterface<MasUsersHistoryDTO> { 

	int getId();

	void setId(int id);

	String getUseUserKey();

	void setUseUserKey(String useUserKey);

	String getUseUcdlogin();

	void setUseUcdlogin(String useUcdlogin);

	String getUseEmployeeId();

	void setUseEmployeeId(String useEmployeeId);

	String getUseFirstName();

	void setUseFirstName(String useFirstName);

	String getUseMiddleName();

	void setUseMiddleName(String useMiddleName);

	String getUseLastName();

	void setUseLastName(String useLastName);

	String getUseMothraId();

	void setUseMothraId(String useMothraId);

	String getUseUserTitleKey();

	void setUseUserTitleKey(String useUserTitleKey);

	String getUseDepartmentKey();

	void setUseDepartmentKey(String useDepartmentKey);

	String getUsePhone();

	void setUsePhone(String usePhone);

	String getUseFax();

	void setUseFax(String useFax);

	String getUseEmail();

	void setUseEmail(String useEmail);

	boolean isUseIsActive();

	void setUseIsActive(boolean useIsActive);

	LocalDateTime getUseStartDate();

	void setUseStartDate(LocalDateTime useStartDate);

	LocalDateTime getUseEndDate();

	void setUseEndDate(LocalDateTime useEndDate);

	String getUseUpdatedBy();

	void setUseUpdatedBy(String useUpdatedBy);

}