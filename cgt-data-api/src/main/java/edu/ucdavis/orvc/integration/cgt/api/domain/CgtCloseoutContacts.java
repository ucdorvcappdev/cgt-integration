package edu.ucdavis.orvc.integration.cgt.api.domain;



import edu.ucdavis.orvc.integration.cgt.api.domain.dto.CgtCloseoutContactsDTO;

public interface CgtCloseoutContacts extends CgtBaseInterface<CgtCloseoutContactsDTO> { 

	CgtCloseoutContactsId getId();

	void setId(CgtCloseoutContactsId id);

}