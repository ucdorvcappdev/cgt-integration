package edu.ucdavis.orvc.integration.cgt.api.domain;

import edu.ucdavis.orvc.integration.cgt.api.domain.dto.CgtWrapUpCloseRulesToDepartmentsDTO;

public interface CgtWrapUpCloseRulesToDepartments extends CgtBaseInterface<CgtWrapUpCloseRulesToDepartmentsDTO> { 

	int getW2sRuleKey();

	void setW2sRuleKey(int w2sRuleKey);

	int getW2sCloseRuleKey();

	void setW2sCloseRuleKey(int w2sCloseRuleKey);

	String getW2sNameBeginsWith();

	void setW2sNameBeginsWith(String w2sNameBeginsWith);

	String getW2sBoucode();

	void setW2sBoucode(String w2sBoucode);

	String getW2sSponsorSubAgencyKey();

	void setW2sSponsorSubAgencyKey(String w2sSponsorSubAgencyKey);

	int getW2sResultSubmissionTypeKey();

	void setW2sResultSubmissionTypeKey(int w2sResultSubmissionTypeKey);

}