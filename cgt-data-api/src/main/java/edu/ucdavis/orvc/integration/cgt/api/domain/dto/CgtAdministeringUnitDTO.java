package edu.ucdavis.orvc.integration.cgt.api.domain.dto;


import org.joda.time.LocalDateTime;

import edu.ucdavis.orvc.integration.cgt.api.domain.CgtAdministeringUnit;
import edu.ucdavis.orvc.integration.cgt.api.domain.CgtAdministeringUnitRole;
import edu.ucdavis.orvc.integration.cgt.api.domain.CgtProject;
import edu.ucdavis.orvc.integration.cgt.api.domain.MasDepartment;

/**
 * Base Hibernate POJO for CGT.
 *
 *
 **/
public class CgtAdministeringUnitDTO implements CgtAdministeringUnit {

	private static final long serialVersionUID = 8115581249771036328L;

	private int aduAdministeringUnitKey;
	private String aduDepartmentKey;
	private Integer aduUnitRoleKey;
	private String aduProjectKey;
	private String aduOpaccount;
	private boolean aduIsActive;
	private String aduUpdatedBy;
	private LocalDateTime aduDateUpdated;
	private LocalDateTime aduDateCreated;

	private MasDepartmentDTO department;
	private CgtAdministeringUnitRoleDTO administeringUnitRole;
	private CgtProjectDTO project;
	
	

	public CgtAdministeringUnitDTO() {
	}

	public CgtAdministeringUnitDTO(final int aduAdministeringUnitKey, final String aduProjectKey, final boolean aduIsActive,
			final String aduUpdatedBy, final LocalDateTime aduDateUpdated, final LocalDateTime aduDateCreated) {
		this.aduAdministeringUnitKey = aduAdministeringUnitKey;
		this.aduProjectKey = aduProjectKey;
		this.aduIsActive = aduIsActive;
		this.aduUpdatedBy = aduUpdatedBy;
		this.aduDateUpdated = aduDateUpdated;
		this.aduDateCreated = aduDateCreated;
	}

	public CgtAdministeringUnitDTO(final int aduAdministeringUnitKey, final String aduDepartmentKey, final Integer aduUnitRoleKey,
			final String aduProjectKey, final String aduOpaccount, final boolean aduIsActive, final String aduUpdatedBy, final LocalDateTime aduDateUpdated,
			final LocalDateTime aduDateCreated) {
		this.aduAdministeringUnitKey = aduAdministeringUnitKey;
		this.aduDepartmentKey = aduDepartmentKey;
		this.aduUnitRoleKey = aduUnitRoleKey;
		this.aduProjectKey = aduProjectKey;
		this.aduOpaccount = aduOpaccount;
		this.aduIsActive = aduIsActive;
		this.aduUpdatedBy = aduUpdatedBy;
		this.aduDateUpdated = aduDateUpdated;
		this.aduDateCreated = aduDateCreated;
	}

	public CgtAdministeringUnitDTO(CgtAdministeringUnit fromObj) {
		this.aduAdministeringUnitKey = fromObj.getAduAdministeringUnitKey();
		this.aduDepartmentKey = fromObj.getAduDepartmentKey();
		this.aduUnitRoleKey = fromObj.getAduUnitRoleKey();
		this.aduProjectKey = fromObj.getAduProjectKey();
		this.aduOpaccount = fromObj.getAduOpaccount();
		this.aduIsActive = fromObj.isAduIsActive();
		this.aduUpdatedBy = fromObj.getAduUpdatedBy();
		this.aduDateUpdated = fromObj.getAduDateUpdated();
		this.aduDateCreated = fromObj.getAduDateCreated();
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtAdministeringUnit#getAduAdministeringUnitKey()
	 */
	@Override
	
	
	public int getAduAdministeringUnitKey() {
		return this.aduAdministeringUnitKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtAdministeringUnit#setAduAdministeringUnitKey(int)
	 */
	@Override
	public void setAduAdministeringUnitKey(final int aduAdministeringUnitKey) {
		this.aduAdministeringUnitKey = aduAdministeringUnitKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtAdministeringUnit#getAduDepartmentKey()
	 */
	@Override
	
	public String getAduDepartmentKey() {
		return this.aduDepartmentKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtAdministeringUnit#setAduDepartmentKey(java.lang.String)
	 */
	@Override
	public void setAduDepartmentKey(final String aduDepartmentKey) {
		this.aduDepartmentKey = aduDepartmentKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtAdministeringUnit#getAduUnitRoleKey()
	 */
	@Override
	
	public Integer getAduUnitRoleKey() {
		return this.aduUnitRoleKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtAdministeringUnit#setAduUnitRoleKey(java.lang.Integer)
	 */
	@Override
	public void setAduUnitRoleKey(final Integer aduUnitRoleKey) {
		this.aduUnitRoleKey = aduUnitRoleKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtAdministeringUnit#getAduProjectKey()
	 */
	@Override
	
	public String getAduProjectKey() {
		return this.aduProjectKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtAdministeringUnit#setAduProjectKey(java.lang.String)
	 */
	@Override
	public void setAduProjectKey(final String aduProjectKey) {
		this.aduProjectKey = aduProjectKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtAdministeringUnit#getAduOpaccount()
	 */
	@Override
	
	public String getAduOpaccount() {
		return this.aduOpaccount;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtAdministeringUnit#setAduOpaccount(java.lang.String)
	 */
	@Override
	public void setAduOpaccount(final String aduOpaccount) {
		this.aduOpaccount = aduOpaccount;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtAdministeringUnit#isAduIsActive()
	 */
	@Override
	
	public boolean isAduIsActive() {
		return this.aduIsActive;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtAdministeringUnit#setAduIsActive(boolean)
	 */
	@Override
	public void setAduIsActive(final boolean aduIsActive) {
		this.aduIsActive = aduIsActive;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtAdministeringUnit#getAduUpdatedBy()
	 */
	@Override
	
	public String getAduUpdatedBy() {
		return this.aduUpdatedBy;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtAdministeringUnit#setAduUpdatedBy(java.lang.String)
	 */
	@Override
	public void setAduUpdatedBy(final String aduUpdatedBy) {
		this.aduUpdatedBy = aduUpdatedBy;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtAdministeringUnit#getAduDateUpdated()
	 */
	@Override
	
	
	public LocalDateTime getAduDateUpdated() {
		return this.aduDateUpdated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtAdministeringUnit#setAduDateUpdated(org.joda.time.LocalDateTime)
	 */
	@Override
	public void setAduDateUpdated(final LocalDateTime aduDateUpdated) {
		this.aduDateUpdated = aduDateUpdated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtAdministeringUnit#getAduDateCreated()
	 */
	@Override
	
	
	public LocalDateTime getAduDateCreated() {
		return this.aduDateCreated;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtAdministeringUnit#setAduDateCreated(org.joda.time.LocalDateTime)
	 */
	@Override
	public void setAduDateCreated(final LocalDateTime aduDateCreated) {
		this.aduDateCreated = aduDateCreated;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + aduAdministeringUnitKey;
		result = prime * result + ((aduDateCreated == null) ? 0 : aduDateCreated.hashCode());
		result = prime * result + ((aduDateUpdated == null) ? 0 : aduDateUpdated.hashCode());
		result = prime * result + ((aduDepartmentKey == null) ? 0 : aduDepartmentKey.hashCode());
		result = prime * result + (aduIsActive ? 1231 : 1237);
		result = prime * result + ((aduOpaccount == null) ? 0 : aduOpaccount.hashCode());
		result = prime * result + ((aduProjectKey == null) ? 0 : aduProjectKey.hashCode());
		result = prime * result + ((aduUnitRoleKey == null) ? 0 : aduUnitRoleKey.hashCode());
		result = prime * result + ((aduUpdatedBy == null) ? 0 : aduUpdatedBy.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(final Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (!(obj instanceof CgtAdministeringUnitDTO))
			return false;
		final CgtAdministeringUnitDTO other = (CgtAdministeringUnitDTO) obj;
		if (aduAdministeringUnitKey != other.aduAdministeringUnitKey)
			return false;
		if (aduDateCreated == null) {
			if (other.aduDateCreated != null)
				return false;
		} else if (!aduDateCreated.equals(other.aduDateCreated))
			return false;
		if (aduDateUpdated == null) {
			if (other.aduDateUpdated != null)
				return false;
		} else if (!aduDateUpdated.equals(other.aduDateUpdated))
			return false;
		if (aduDepartmentKey == null) {
			if (other.aduDepartmentKey != null)
				return false;
		} else if (!aduDepartmentKey.equals(other.aduDepartmentKey))
			return false;
		if (aduIsActive != other.aduIsActive)
			return false;
		if (aduOpaccount == null) {
			if (other.aduOpaccount != null)
				return false;
		} else if (!aduOpaccount.equals(other.aduOpaccount))
			return false;
		if (aduProjectKey == null) {
			if (other.aduProjectKey != null)
				return false;
		} else if (!aduProjectKey.equals(other.aduProjectKey))
			return false;
		if (aduUnitRoleKey == null) {
			if (other.aduUnitRoleKey != null)
				return false;
		} else if (!aduUnitRoleKey.equals(other.aduUnitRoleKey))
			return false;
		if (aduUpdatedBy == null) {
			if (other.aduUpdatedBy != null)
				return false;
		} else if (!aduUpdatedBy.equals(other.aduUpdatedBy))
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return String.format(
				"CgtAdministeringUnits [aduAdministeringUnitKey=%s, aduDepartmentKey=%s, aduUnitRoleKey=%s, aduProjectKey=%s, aduOpaccount=%s, aduIsActive=%s, aduUpdatedBy=%s, aduDateUpdated=%s, aduDateCreated=%s]",
				aduAdministeringUnitKey, aduDepartmentKey, aduUnitRoleKey, aduProjectKey, aduOpaccount, aduIsActive,
				aduUpdatedBy, aduDateUpdated, aduDateCreated);
	}

	
	
	public MasDepartmentDTO getDepartmentImpl() {
		return department;
	}

	public void setDepartmentImpl(MasDepartmentDTO department) {
		this.department = department;
	}

	
	
	public CgtAdministeringUnitRoleDTO getAdministeringUnitRoleImpl() {
		return administeringUnitRole;
	}

	public void setAdministeringUnitRoleImpl(CgtAdministeringUnitRoleDTO administeringUnitRole) {
		this.administeringUnitRole = administeringUnitRole;
	}
	
	
	
	public CgtProjectDTO getProjectImpl() {
		return project;
	}

	public void setProjectImpl(CgtProjectDTO project) {
		this.project = project;
	}

	
	
	
	@Override
	
	public MasDepartment getDepartment() {
		return this.department;
	}

	@Override
	public void setDepartment(MasDepartment department) {
		this.department = (MasDepartmentDTO) department;
	}

	@Override
	
	public CgtAdministeringUnitRole getAdministeringUnitRole() {
		return this.administeringUnitRole;
	}

	@Override
	public void setAdministeringUnitRole(CgtAdministeringUnitRole administeringUnitRole) {
		this.administeringUnitRole = (CgtAdministeringUnitRoleDTO) administeringUnitRole;
	}

	@Override
	
	public CgtProject getProject() {
		return this.project;
	}

	@Override
	public void setProject(CgtProject project) {
		this.project = (CgtProjectDTO) project;
	}

	@Override
	public CgtAdministeringUnitDTO toDTO() {
		return new CgtAdministeringUnitDTO(this);
	}

}
