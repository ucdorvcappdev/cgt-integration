package edu.ucdavis.orvc.integration.cgt.api.domain.dto;


import edu.ucdavis.orvc.integration.cgt.api.domain.CgtWrapUpCloseRulesToDfitypes;
import edu.ucdavis.orvc.integration.cgt.api.domain.CgtWrapUpCloseRulesToDfitypesId;

/**
 * Base Hibernate POJO for CGT.
 *
 *
 **/


public class CgtWrapUpCloseRulesToDfitypesDTO implements CgtWrapUpCloseRulesToDfitypes {

	private static final long serialVersionUID = 2538086362932402398L;
	private CgtWrapUpCloseRulesToDfitypesIdDTO id;
	private int w2dResultSubmissionTypeKey;

	public CgtWrapUpCloseRulesToDfitypesDTO(final CgtWrapUpCloseRulesToDfitypes fromObj) {



	}

	public CgtWrapUpCloseRulesToDfitypesDTO() {
	}

	public CgtWrapUpCloseRulesToDfitypesDTO(final CgtWrapUpCloseRulesToDfitypesIdDTO id, final int w2dResultSubmissionTypeKey) {
		this.id = id;
		this.w2dResultSubmissionTypeKey = w2dResultSubmissionTypeKey;
	}

	public CgtWrapUpCloseRulesToDfitypesIdDTO getIdImpl() {
		return this.id;
	}

	public void setIdImpl(final CgtWrapUpCloseRulesToDfitypesIdDTO id) {
		this.id = id;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtWrapUpCloseRulesToDfitypes#getW2dResultSubmissionTypeKey()
	 */
	@Override
	
	public int getW2dResultSubmissionTypeKey() {
		return this.w2dResultSubmissionTypeKey;
	}

	/* (non-Javadoc)
	 * @see edu.ucdavis.orvc.integration.cgt.domain.CgtWrapUpCloseRulesToDfitypes#setW2dResultSubmissionTypeKey(int)
	 */
	@Override
	public void setW2dResultSubmissionTypeKey(final int w2dResultSubmissionTypeKey) {
		this.w2dResultSubmissionTypeKey = w2dResultSubmissionTypeKey;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + w2dResultSubmissionTypeKey;
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(final Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (!(obj instanceof CgtWrapUpCloseRulesToDfitypesDTO))
			return false;
		final CgtWrapUpCloseRulesToDfitypesDTO other = (CgtWrapUpCloseRulesToDfitypesDTO) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (w2dResultSubmissionTypeKey != other.w2dResultSubmissionTypeKey)
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return String.format("CgtWrapUpCloseRulesToDfitypes [id=%s, w2dResultSubmissionTypeKey=%s]", id,
				w2dResultSubmissionTypeKey);
	}

	@Override
	
	public CgtWrapUpCloseRulesToDfitypesId getId() {
		return id;
	}

	@Override
	public void setId(CgtWrapUpCloseRulesToDfitypesId id) {
		this.id = (CgtWrapUpCloseRulesToDfitypesIdDTO) id;
	}

	public CgtWrapUpCloseRulesToDfitypesDTO toDTO() {
		return new CgtWrapUpCloseRulesToDfitypesDTO(this);
	}
}
